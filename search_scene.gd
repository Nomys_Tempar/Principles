extends Node

func _ready():
	var place_name_ref1: String = _reformating_place_name(PlayerData.player_current_place)
	var place = ConfigFile.new()
	place.load("res://game_data/places_data/"+place_name_ref1+".cfg")
	var player_search_stat: int = (int(PlayerData.stats_concat.Strategy)+int(PlayerData.stats_concat.Will))
	get_tree().get_nodes_in_group("player")[2].on_player_entry_text_submitted(str(PlayerData.player_name+" is searching for something..."), true, "Room")
	if place.get_value("Location Datas", "search_threshold") < player_search_stat : ## Search threshold as to be lower than player's Strategy+Will stats
		get_tree().get_nodes_in_group("player")[2].on_player_entry_text_submitted(place.get_value("Location Datas", "search_success"), true, "Room") ## Display narration text in player's livefeed
		for i in place.get_value("Location Datas", "place_leads_to").size() :
			if "[hidden]" in place.get_value("Location Datas", "place_leads_to")[i]:
				PlayerData.player_known_places.append(place.get_value("Location Datas", "place_leads_to")[i])
	else:
		get_tree().get_nodes_in_group("player")[2].on_player_entry_text_submitted(place.get_value("Location Datas", "search_fail"), true, "Room") ## Display narration text in player's livefeed
	PlayerSave.savegame()
	queue_free()


### Reformating place_name if needed (adding "_" to remove spaces)
func _reformating_place_name(current_place: String) -> String:
	var place_name_ref:String
	var place_name_ref1:String
	if " " in current_place :
		place_name_ref = current_place.replace(" ", "_")
	else:
		place_name_ref = current_place
	if "'" in place_name_ref :
		place_name_ref1 = place_name_ref.replace("'", "_")
	else:
		place_name_ref1 = place_name_ref

	return place_name_ref1.to_lower()
### Reformating place_name if needed (adding "_" to remove spaces)
