extends Node # Global variables of the game 

### Dev mode
var dev:bool = true ## if true / network is localhost
var version:float = 0.8 ## Current version number 

### Network and server variables
var server:int = 0 ## Server presence variable : 1 is for server, 0 or other numbers for client
#var players_searching = [] # Array of players searching for dials
var character_unique_id: String
var client_connected:Array = [] ## Array of client's ids
var players_ingame:Array = [] ## Array of players'id currently ingame (for management of disconnecting players)
#var ingame = false
var login_done: bool = false ## Variable for getting a disconnect option in the escape menu
#var online_players = 0 # Var for the total number of players in currently in game
#var client_connected = 0 # Var to check if clients are connected to the server

### Server setup variables
var combat_allowed: bool = true ## Confrontations allowed in game
var confrontation_timer: int = 100 ## Confrontation round time duration (43200 for 24h)

### Files paths
## See connection.gd for directories handling
var online_players_file_path: String = "user://save/server/online_players.dat" ## File to track online players
var players_fighting_file_path: String = "user://save/server/players_fighting.dat" ## File to track players in fights
var players_cuis_file_path: String = "user://save/server/players_cuis.dat" ## File to match multiplayer ids with character unique ids (cui)

### Place's variables
var main_places_infos:Array ## place_name, place_type, place_rooms
var place_infos_text
var place_leads_to

#### Logs variables
#var log_name = "log_%d"
#var log_selected = 0
#var log_num = log_name % log_selected
#var button_logs # Log number unlock by a button
#var dials_logs # Variable for saving dials
#var log_state # 1 for history, 2 for dials, 3 for makers

#### Charts variables
#var charts # Dictionnary {subject : [player_group : [ans], [ans, text], etc]}
#
#### GM Section
var points_to_add_at_creation:int = 10 ## Points players can add to stats or to pick more skills/actions
var default_place_and_room:Array = ["start_place", "Path"] ## Array defining the default starting place and room for a new player. Place name is the name of the place file without extention and room name is the name inside the file
#var maker_section = false
#var player_group # It's a string because we need to use it as a key in a dictionary
#var player_id_in_group # Array of players of one group's ids
#var maker_entry
#var current_subject = ["null","null","null"] # Array identifying subject [Q number string, question string, type string]
#var current_topic_time

### Sound variables
var logs_sound:bool = false ## If true trigger a log display sound
var sound_buttons:int = 0 ## State variable for buttons sound management : 0 = no sound, 
