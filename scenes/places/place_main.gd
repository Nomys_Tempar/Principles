extends Node
## Main place scene, handling of most of the places caracteristics

var place_name:String
var place_type
var place_id
var place_rooms:Array
#var last_pj_date
#var coordinates
#var magic_tech
#var sky
#var habitable
#var human
#var humanlike
#var animal
#var vegetal
#var mineral
#var politic
#var belligerent
#var time_differential
#var local_time

var location_dict
#var loc_name
var loc_type
var loc_rooms
var loc_id
#var player_movement

const loc_visuals = preload("res://scenes/places/place_visuals.tscn") # Scene to display image of the place and rooms
const loc_infos = preload("res://scenes/places/place_infos.tscn") # Scene with romms and characters currently in them
const travel_handling = preload("res://scenes/places/travel_place_handling.tscn") # Scene loaded to handle player movement between place and/or rooms
var place_music

func _ready() -> void :
	### Loading the current place
	var location_to_load = ConfigFile.new()
	location_to_load.load("res://game_data/places_data/"+PlayerData.player_current_place+".cfg")

	var section: String = location_to_load.get_sections()[0]
	place_name = location_to_load.get_value(section, "name")
	place_type = location_to_load.get_value(section, "type")
	place_rooms = location_to_load.get_value(section, "room")
	place_id = location_to_load.get_value(section, "id")

	global.main_places_infos = [place_name, place_type, place_rooms]
	global.place_infos_text = location_to_load.get_value(section, "place_text")
	global.place_leads_to = location_to_load.get_value(section, "place_leads_to")

	### Naming the place
	get_node("./vplace_title/place_name").set_text(place_name)

	#_travel_handling_scene()

	if multiplayer.get_unique_id() != 1 :
		get_parent().get_node("travel_place_handling").player_arriving(place_name)
		_create_visual_scene()
		
		### Handling place music
		var music_dir = DirAccess.open("res://game_data/places_data")
		if music_dir.file_exists("res://game_data/places_data/"+PlayerData.player_current_place+".ogg") == true:
			get_node("place_music").stream = load("res://game_data/places_data/"+PlayerData.player_current_place+".ogg")
			get_node("place_music").play()
		### Handling place music
	else:
		create_info_scene()


func _travel_handling_scene() -> void :
	var travel_handling_scene = travel_handling.instantiate() 
	add_child(travel_handling_scene)


func _create_visual_scene() -> void :
	var loc_visuals_scene = loc_visuals.instantiate() 
	$vplace_title/vplace.add_child(loc_visuals_scene)


func create_info_scene() -> void :
	var loc_infos_scene = loc_infos.instantiate() 
	$vplace_title/vplace.add_child(loc_infos_scene)


### Func handling the traveling between places
func change_place(new_place) -> void :
	PlayerData.player_current_place = new_place
	PlayerData.player_current_room = null
	_setup_new_place()


func _setup_new_place() -> void : # when the node is dismiss (by changing place or quitting game) the player notify other players in the current place
	if multiplayer.is_server() == false:
		for i in PlayerData.player_place_group.size(): # Here the player delete himself from the player_place_group
			for y in PlayerData.player_place_group[i][1].size():
				if PlayerData.player_place_group[i][1][y][0] == PlayerData.player_name :
					PlayerData.player_place_group[i][1].remove_at(y)
				else:pass
				y += 1
			i += 1

		for i in PlayerData.player_place_group.size(): # Then send it to other players so they can update rooms of the place
			for y in PlayerData.player_place_group[i][1].size():
				if PlayerData.player_place_group[i][1][y][0] == PlayerData.player_name :pass
				else:
					rpc_id(int(PlayerData.player_place_group[i][1][y][1]), "_update_room", PlayerData.player_place_group)
				y += 1
			i += 1
		set_name("previous_place") # Changing node's name to bypass unique naming issue
		get_node("..").get_parent().get_parent().get_parent().change_place() # Recreate place_main scene
		queue_free() # Destroy the previous place


@rpc("any_peer", "call_remote", "reliable")
func _update_room(new_player_place_group) -> void : # Other players update rooms in the place
	print("change_done")
	get_node("vplace_title/vplace/place_infos").update_room(new_player_place_group)
