extends Node

### Funcs to handle player arrival in a place
func player_arriving(place_name: String) -> void : ## Func to annunce player's arrival to the sever
	rpc_id(1, "_new_player_in_place", place_name, PlayerData.player_current_room, PlayerData.player_name)


@rpc("any_peer", "call_remote", "reliable")
func _new_player_in_place(place_name: String, room_name, player_name: String) -> void : ## Server save the new arrival
	var player_id:int = multiplayer.get_remote_sender_id()
	_place_player_tracking(player_id, player_name, place_name, room_name)


func _place_player_tracking(player_id: int, player_name: String, place_name: String, room_name) -> void : ## Func to track players in places and rooms in a file (every place have is own file) - Server Side
	var place_name_ref1: String = _reformating_place_name(place_name) # Reformating place_name
	var place_dir = DirAccess.open("user://save/server/places_player_tracking")
	var update_place = ConfigFile.new()
	if  place_dir.file_exists("user://save/server/places_player_tracking/"+place_name_ref1+".dat"):
		## Open and load the file
		update_place.load("user://save/server/places_player_tracking/"+place_name_ref1+".dat")

		var new_player_array: Array = [player_name, player_id]
		if room_name == null :
			var temp_room_array: Array = update_place.get_value(update_place.get_sections()[0], "players")
			temp_room_array.append(new_player_array)
			update_place.set_value(update_place.get_sections()[0], "players", temp_room_array)
		else:
			for i in update_place.get_sections().size():
				if update_place.get_sections()[i] == room_name :
					var temp_room_array: Array = update_place.get_value(room_name, "players").duplicate(true)
					temp_room_array.append(new_player_array)
					update_place.set_value(room_name, "players", temp_room_array)
	else:
		### Creating the current place
		var place = ConfigFile.new()
		place.load("res://game_data/places_data/"+place_name_ref1+".cfg")

		### Create the new tracking file
		var place_rooms: Array = place.get_value("Location Datas", "room")
		for i in place_rooms.size():
			if place_rooms[i] is String :
				if room_name == null :
					if i == 1 :
						update_place.set_value(place_rooms[i], "players", [[player_name, player_id]])
					else:
						update_place.set_value(place_rooms[i], "players", [])
				else:
					if place_rooms[i].matchn(room_name):
						update_place.set_value(room_name, "players", [[player_name, player_id]])
					else:
						update_place.set_value(place_rooms[i], "players", [])
		### Create the new tracking file

	## Writing new datas
	update_place.save("user://save/server/places_player_tracking/"+place_name_ref1+".dat")

	_prepare_the_array(player_id, update_place)

func _prepare_the_array(player_id: int, update_place):
	### Construct current_place_players array of arrays to send it to the player
	var current_place_players: Array = [] # [[room_name, [[player_name, player_id], [player_name, player_id], [room_name, [[player_name, player_id], [player_name, player_id]]]
	for i in update_place.get_sections().size():
		current_place_players.append([update_place.get_sections()[i], update_place.get_value(update_place.get_sections()[i], "players")])
	_send_the_array_back(player_id, current_place_players)


func _send_the_array_back(player_id: int, current_place_players):
	print(current_place_players)
	### Server send back the array
	for i in current_place_players.size():
		for y in current_place_players[i][1].size():
			if current_place_players[i][1][y][1] == player_id :
				rpc_id(player_id, "_receiving_place_and_rooms_players_and_infos", current_place_players)
			else:
				rpc_id(int(current_place_players[i][1][y][1]), "_receiving_new_player", current_place_players)


func deleting_player_from_room_group(id: int) : ## When player quit the game or disconnect, Server removes him from the place (access from network.gd)
	var old_players:Array
	var dir = DirAccess.open("user://save/server/places_player_tracking/")
	var tracking_files: Array = dir.get_files()
	for i in tracking_files.size(): ## Server search player in places and rooms
		var file = ConfigFile.new()
		file.load("user://save/server/places_player_tracking/"+tracking_files[i])
		var sections: Array = file.get_sections()
		for y in sections.size():
			var players_in_location: Array = file.get_value(sections[y], "players")
			for x in players_in_location.size():
				old_players = players_in_location.duplicate(true)
				if old_players[x][1] == id : ## Server finds quitting player and removes him of the room
					players_in_location.remove_at(x)
					file.save("user://save/server/places_player_tracking/"+tracking_files[i])
					_prepare_the_array(id, file) ## Sending to prep the place array and update room and place for players still here


@rpc("authority", "call_remote", "reliable")
func _receiving_place_and_rooms_players_and_infos(current_place_players: Array) -> void : # Player receive place and rooms players
	for i in current_place_players.size():
		if current_place_players[i][0] == PlayerData.player_current_room :
			PlayerData.player_room_group = current_place_players[i][1]
	PlayerData.player_place_group = current_place_players
	PlayerData.player_known_places.append(PlayerData.player_current_place)
	get_parent().get_node("place_main").create_info_scene()


@rpc("authority", "call_remote", "reliable")
func _receiving_new_player(current_place_players: Array) -> void : # Other players updating rooms
	PlayerData.player_place_group = current_place_players
	get_parent().get_node("./place_main/vplace_title/vplace/place_infos").updating_rooms()
### Funcs to handle player arrival in a place


### From Place_infos handling functions ################################################
func change_room(_current_room_number, room_clicked) -> void :
	rpc_id(1, "_server_update_rooms", global.main_places_infos[0], room_clicked) # Server get the change for room


### Server funcs to update rooms
@rpc("any_peer", "call_remote", "reliable")
func _server_update_rooms(current_place, room_clicked) -> void :
	var player_id:int = multiplayer.get_remote_sender_id()
	var place_name_ref1: String = _reformating_place_name(current_place) # Reformating current_place name
	var update_place = ConfigFile.new()

	update_place.load("user://save/server/places_player_tracking/"+place_name_ref1+".dat")

	## Remove player from the room he left
	var player_array: Array
	var sections:Array = update_place.get_sections()
	for i in sections.size():
		var array_of_player_arrays: Array = update_place.get_value(sections[i], "players").duplicate(true)
		for x in update_place.get_value(sections[i], "players").size():
			#var array_of_player_arrays: Array = update_place.get_value(sections[i], "players")
			print(array_of_player_arrays)
			if update_place.get_value(sections[i], "players")[x][1] == player_id :
				player_array = array_of_player_arrays[x].duplicate(true) ## Get the player array
				array_of_player_arrays.remove_at(x)
		update_place.set_value(sections[i], "players", array_of_player_arrays)

	## Adding player in new room if he's staying in the same place
	if room_clicked is int :
		var temp_players_array:Array = update_place.get_value(sections[room_clicked], "players").duplicate(true)
		temp_players_array.append(player_array)
		update_place.set_value(sections[room_clicked], "players", temp_players_array)

	### Writing new datas
	update_place.save("user://save/server/places_player_tracking/"+place_name_ref1+".dat")

	## Construct current_place_players array of arrays to send it to the player
	var current_place_players: Array = [] # [[room_name, [[player_name, player_id], [player_name, player_id], [room_name, [[player_name, player_id], [player_name, player_id]]]
	for i in update_place.get_sections().size():
		current_place_players.append([update_place.get_sections()[i], update_place.get_value(update_place.get_sections()[i], "players")])

	if room_clicked is String :
		_sending_new_rooms(player_id, current_place_players, "leave_place")
	else:
		_sending_new_rooms(player_id, current_place_players, "change_room")


func _sending_new_rooms(player_id: int, new_player_place_group: Array, player_movement: String) -> void :
	rpc_id(player_id, "_update_completed_serverside_notice", new_player_place_group, player_movement)
## Server funcs to update rooms


@rpc("authority", "call_remote", "reliable")
func _update_completed_serverside_notice(new_player_place_group: Array, player_movement: String) -> void :
	get_parent().get_node("place_main/vplace_title/vplace/place_infos").update_completed_serverside(new_player_place_group, player_movement)
### Place_infos handling functions ###################################################


### Reformating place_name if needed (adding "_" to remove spaces)
func _reformating_place_name(current_place: String) -> String:
	var place_name_ref:String
	var place_name_ref1:String
	if " " in current_place :
		place_name_ref = current_place.replace(" ", "_")
	else:
		place_name_ref = current_place
	if "'" in place_name_ref :
		place_name_ref1 = place_name_ref.replace("'", "_")
	else:
		place_name_ref1 = place_name_ref

	return place_name_ref1.to_lower()
### Reformating place_name if needed (adding "_" to remove spaces)

func _exit_tree(): ## DEBUG
	print("crash "+str(multiplayer.get_unique_id()))
