extends Node
# Description and infos about the current Place and locations (pj and pnj visible and 

var place_rooms
var current_room_number
var current_room_path

signal room_cleaned

func _ready() -> void :
	get_node("VSplitContainer/infos_container/MC_room0").visible = false
	get_node("VSplitContainer/infos_container/MC_room1").visible = false
	get_node("VSplitContainer/infos_container/MC_room2").visible = false
	get_node("VSplitContainer/infos_container/MC_room3").visible = false
	
	while global.main_places_infos == null:
		pass

	place_rooms = global.main_places_infos[2]

	get_node("VSplitContainer/infos/place_infos").set_text(global.place_infos_text[0])

	# Name rooms from the place's data
	for i in place_rooms[0]:
		get_node("VSplitContainer/infos_container/MC_room"+str(i)).visible = true
		get_node("VSplitContainer/infos_container/MC_room"+str(i)+"/loc"+str(i)).set_text(place_rooms[i+1])
		i += 1

	if global.server == 1 :
		pass
	else:
		_enter_room()


func _enter_room() -> void : # Entering func
	_change_notice("change_room")


func change_room(room_clicked) -> void :
	if room_clicked is int : # Rooms buttons handling
		if current_room_number != room_clicked:
			get_node("VSplitContainer/infos_container/MC_room"+str(current_room_number)).get_child(0).disabled = false
		else:
			updating_rooms()
	else: # room_clicked is a string if the player leave the place
		get_node("VSplitContainer/infos_container/MC_room"+str(current_room_number)).get_child(0).disabled = false
	
	get_parent().get_parent().get_parent().get_parent().get_node("travel_place_handling").change_room(current_room_number, room_clicked) ## Here we go to travel_place_handling scene to reach the server


func update_completed_serverside(new_player_place_group: Array, player_movement: String) -> void : ## Here we get back from travel_place_handling scene
	PlayerData.player_place_group = new_player_place_group
	_change_notice(player_movement)


func _change_notice(player_movement: String) -> void :
	### Sending the change to all players in the place
	for i in PlayerData.player_place_group.size():
		for y in PlayerData.player_place_group[i][1].size():
			if PlayerData.player_place_group[i][1][y][0] == PlayerData.player_name :
				PlayerData.player_current_room = PlayerData.player_place_group[i][0]
				updating_rooms()
			else:
				if player_movement != "leave_place":
					rpc_id(int(PlayerData.player_place_group[i][1][y][1]), "update_room", PlayerData.player_place_group)
				else:pass
			y += 1
		i += 1
	if player_movement == "leave_place":
		emit_signal("room_cleaned")


@rpc ("any_peer", "call_remote", "reliable")
func update_room(new_player_place_group) -> void : ## When a new player arrives or a player changes room, we update the display of all players
	PlayerData.player_place_group = new_player_place_group ## Updating player_place_group var

	### Getting the new global.player_current_room
	for i in PlayerData.player_place_group.size():
		for y in PlayerData.player_place_group[i][1].size():
			if PlayerData.player_place_group[i][1][y][0] == PlayerData.player_name :
				PlayerData.player_current_room = PlayerData.player_place_group[i][0]
	updating_rooms()


func updating_rooms() -> void :
	### Clearing rooms
	get_node("VSplitContainer/infos_container/MC_room0/list_loc0").clear()
	get_node("VSplitContainer/infos_container/MC_room1/list_loc1").clear()
	get_node("VSplitContainer/infos_container/MC_room2/list_loc2").clear()
	get_node("VSplitContainer/infos_container/MC_room3/list_loc3").clear()
	### Enabling all rooms buttons
	get_node("VSplitContainer/infos_container/MC_room0/loc0").disabled = false
	get_node("VSplitContainer/infos_container/MC_room1/loc1").disabled = false
	get_node("VSplitContainer/infos_container/MC_room2/loc2").disabled = false
	get_node("VSplitContainer/infos_container/MC_room3/loc3").disabled = false

	### Reloading players's names into the rooms
	for i in PlayerData.player_place_group.size():
		for y in PlayerData.player_place_group[i][1].size() :
			var room = get_node("VSplitContainer/infos_container/MC_room"+str(i))
			room.get_child(1).add_item(PlayerData.player_place_group[i][1][y][0]) ## PlayerData.player_place_group is an arrays of arrays inside arrays
			room.get_child(1).set_item_metadata(room.get_child(1).get_item_count()-1, PlayerData.player_place_group[i][1][y][1])
			if PlayerData.player_place_group[i][1][y][0] == PlayerData.player_name : ## We disable room button if the player is in this room
				current_room_number = i
				current_room_path = room.get_child(1)
#				global.player_room_group = global.player_place_group[place_rooms[i+1]]
				room.get_child(0).disabled = true ## Disabling the button of the current room
				get_node("VSplitContainer/infos/room_infos").set_text(global.place_infos_text[i+1])
				get_node("/root/connection/login/pj/main_interface/HSplitContainer/text_panel/vtexts/player_livefeed").asking_for_logs() ## Asking to server the logs of this room
			else :
				y += 1
		i += 1
	PlayerSave.savegame() ## Saving character
