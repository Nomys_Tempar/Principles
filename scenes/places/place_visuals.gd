extends TextureRect

var place_image = load("res://game_data/places_data/"+PlayerData.player_current_place+".png")

func _ready():
	if place_image != null :
		set_texture(place_image)
