extends Control

func _ready() -> void :
	if multiplayer.get_unique_id() == 1:
		queue_free()
	else:
		queue_free()


func _on_pass_intro_pressed() -> void :
	queue_free()
