extends Node
## Login scene for players and game masters

const pj = preload("res://scenes/player/pj.tscn")
const create = preload("res://scenes/player/new_player_creation/intro_new_player.tscn")
# const gm = preload("")

func _ready() -> void :
	if global.server == 1 :
		_instance_pj()
	$name_label/name.grab_focus()


func _instance_pj() -> void :
	var pj_scene = pj.instantiate()
	add_child(pj_scene)


func _on_player_pressed() -> void :
	PlayerData.player_name = $name_label/name.get_text()
	rpc_id(1, "_load_save_verif", get_node("name_label/name").get_text()) # On pressed, we ask the server to load the savegame and we send to server the player id


func _on_game_master_pressed() -> void :
	print("gm_side")
#	var gm_scene = gm.instantiate()
#	add_child(gm_scene)


@rpc("any_peer", "call_remote", "reliable")
func _load_save_verif(player_n: String) -> void : # Server side: retrieving player save
	var save_dir = DirAccess.open("user://save/server/players")
	if not save_dir.file_exists("user://save/server/players/savegame_"+player_n+".dat"):
		rpc_id(multiplayer.get_remote_sender_id(), "_no_save_to_load")

	## Load the file line by line and process into the dictionary to check datas
	#save_game.open_encrypted_with_pass(player_name, File.READ, player_pass)
	#if save_game.is_open() != true : # Here we check if the player's pass matches the one saved
#
##	if player_pass != current_line.player_pass : # Here we check if the player's pass matches the one saved
		#rpc_id(id, "_wrong_pass")
		#return # Error! Player entered the wrong password
		#save_game.close()
	else : # If the password is the same...
		
		### And we save the player in the online_player_data file
		var online_players = ConfigFile.new()
		var online_players_dir = DirAccess.open("user://save/server")
		if not online_players_dir.file_exists("user://save/server/online_players.dat"):
			online_players.set_value("current_online", player_n, multiplayer.get_remote_sender_id())

			online_players.save("user://save/server/online_players.dat")
			### ...we send the remaining variables to the player.
			_server_load_playersave(player_n, multiplayer.get_remote_sender_id())
		else :
			online_players.load("user://save/server/online_players.dat")
			if online_players.has_section_key("current_online", player_n): # Player already online
				rpc_id(multiplayer.get_remote_sender_id(), "_already_online")
			else:
				online_players.set_value("current_online", player_n, multiplayer.get_remote_sender_id())
				online_players.save("user://save/server/online_players.dat")
				### ...we send the remaining variables to the player.
				_server_load_playersave(player_n, multiplayer.get_remote_sender_id())



func _server_load_playersave(player_n: String, player_id: int) -> void : # Once every verification done the server transmit save datas to the player
	var save_file = ConfigFile.new()
	save_file.load("user://save/server/players/savegame_"+player_n+".dat")
	### ...we send the remaining variables to the player.
	
	## Conversion of the ConfigFile into dictionary
	var player_dict: Dictionary
	var player_dict_keys:Dictionary={}
	var player_sections:Array = save_file.get_sections()
	for x in player_sections.size():
		var player_keys:Array = save_file.get_section_keys(player_sections[x])
		for y in player_keys.size():
			player_dict_keys[player_keys[y]] = save_file.get_value(player_sections[x], player_keys[y])
		player_dict[player_sections[x]] = player_dict_keys
	## Conversion of the ConfigFile into dictionary

	if player_dict_keys.in_combat == true : ## If the player is in confrontation Server update his multiplayer id in players_fighting.dat file
		ServerSave.update_player_id_for_players_in_combat(player_n, player_id, player_dict_keys.player_current_place, player_dict_keys.player_current_room)
	rpc_id(player_id, "_loading_player_datas", player_dict_keys)


@rpc("authority", "call_remote", "reliable")
func _no_save_to_load() -> void :
	get_node("name_label/log_label").set_text("Error : no save to restore or invalid name")


@rpc("authority", "call_remote", "reliable")
func _wrong_pass() -> void :
	get_node("name_label/log_label").set_text("Error : wrong password")


@rpc("authority", "call_remote", "reliable")
func _already_online() -> void :
	get_node("name_label/log_label").set_text("Error : player already connected")


### The player receive his datas...
@rpc("authority", "call_remote", "reliable")
func _loading_player_datas(player_data_load: Dictionary) -> void :
	## Receiving variables
	PlayerData.player_name = player_data_load.player_name
	PlayerData.player_sex = player_data_load.player_sex
	PlayerData.player_description = player_data_load.player_description
	PlayerData.player_description_close = player_data_load.player_description_close
	PlayerData.player_background = player_data_load.player_background
	PlayerData.player_class = player_data_load.player_class
	PlayerData.player_posture = player_data_load.player_posture
	PlayerData.stats_concat = player_data_load.player_stats
	PlayerData.player_current_place = player_data_load.player_current_place
	PlayerData.player_current_room = player_data_load.player_current_room
	PlayerData.player_actions = player_data_load.player_actions
	PlayerData.player_maxlife = player_data_load.player_maxlife
	PlayerData.player_currentlife = player_data_load.player_currentlife
	PlayerData.in_combat = player_data_load.in_combat
	PlayerData.opponents = player_data_load.opponents
	PlayerData.player_unconscious = player_data_load.player_unconscious
	PlayerData.player_dead = player_data_load.player_dead

	_launch_game()


func _on_new_player_pressed() -> void :
	var new_player = create.instantiate()
	add_child(new_player)


func verif_new_player_name(nc_name) -> void :
	rpc_id(1, "_server_verif_new_player_name", nc_name)


@rpc("any_peer", "call_remote", "reliable")
func _server_verif_new_player_name(nc_name) -> void : ## Server verify new character name validity
	var id:int = multiplayer.get_remote_sender_id()
	var dir = DirAccess.open("user://save/server/players/")
	dir.list_dir_begin()
	var file = dir.get_next()
	if file.match(nc_name):
		rpc_id(id, "_verif_new_character_done", 1)
	else:
		rpc_id(id, "_verif_new_character_done", 0)


@rpc("authority", "call_remote", "reliable")
func _verif_new_character_done(valid) -> void :
	get_node("intro_new_player/creation_scene1").is_character_exist(valid)


func _launch_game() -> void :
	var pj_scene = pj.instantiate()
	add_child(pj_scene)
