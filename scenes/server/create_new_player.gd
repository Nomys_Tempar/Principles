extends Button

var place_room:String
var player_name:String
var new_player_file

func _on_pressed() -> void :
	player_name = get_node("../name_label/name").get_text()
	rpc_id(1, "_server_create_new_player", multiplayer.get_unique_id(), player_name)


@rpc("any_peer", "call_remote", "reliable")
func _server_create_new_player(new_player_id: int, new_player_name: String) -> void : # Server create the new player's savefile
	### Checking if the save directory exist
	var dir = DirAccess.open("user://save/server")
	if dir.dir_exists("user://save/server/players"):
		pass
	else:
		dir.make_dir("user://save/server/players")
	### Checking if the save directory exist

	if new_player_file.file_exists("user://save/server/players/savegame_"+new_player_name+".dat"):
		rpc_id(new_player_id, "_name_already_taken") # Server has find a player with this name
	else:
		new_player_file = ConfigFile
		new_player_file.set_value("Player Datas", "player_name", new_player_name)
		new_player_file.set_value("Player Datas", "player_sex", "male")
		new_player_file.set_value("Player Datas", "player_current_place", global.default_place_and_room[0])
		new_player_file.set_value("Player Datas", "player_current_room", global.default_place_and_room[1])
		new_player_file.set_value("Player Datas", "player_actions", [["Travel", "Search", "Engage Combat", "Take"],["physics_test"],["spell_test"],["special_test"]])

		new_player_file.save("user://save/server/players/savegame_"+new_player_name+".dat")


@rpc("authority", "call_remote", "reliable")
func _name_already_taken() -> void : # Player has to choose another name
	OS.alert("Name already taken")
