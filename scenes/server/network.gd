extends Node

var SERVER_IP:String
var SERVER_PORT:int
var MAX_PLAYERS:int = 500

func _ready() -> void :
	multiplayer.connect("peer_disconnected",Callable(self,"_player_disconnected"))
	multiplayer.connect("connected_to_server",Callable(self,"_connected_ok"))
	multiplayer.connect("connection_failed",Callable(self,"_connected_fail"))


func server_valid() -> void :
	# TODO : ping to internet and server here
	if global.server == 1:
		SERVER_IP = "localhost"
		SERVER_PORT = 8996
		_connect_server()
	else:
		_connect_client()


func _connect_server() :
	var peer = ENetMultiplayerPeer.new()
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	multiplayer.multiplayer_peer = peer
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to start multiplayer server.")
		return
	get_parent().start_login_and_intro()


func _connect_client() -> void :
	if get_node("ip").get_text() == "" or get_node("port").get_text() == "":
		OS.alert("No IP adress or port specify.")
	else:
		var peer = ENetMultiplayerPeer.new()
		peer.create_client(get_node("ip").get_text(), int(get_node("port").get_text()))
		multiplayer.multiplayer_peer = peer


func _connected_ok() -> void : # Client side
	get_parent().start_login_and_intro()
	#rpc_id(1, "_connection_client")


#@rpc("any_peer", "call_remote", "reliable")
#func _connection_client() -> void :
	#var new_id:int = multiplayer.get_remote_sender_id()
	#global.client_connected.append(new_id)
	#ServerSave.update_online_client_file(new_id)


func _connected_fail() -> void :
	OS.alert("Failed to connect to server.")


func _player_disconnected(id: int) -> void : # Server side
	print(id)
	get_parent().get_node("login/pj/main_interface/HSplitContainer/place_panel/travel_place_handling").deleting_player_from_room_group(id)
	#global.client_connected.erase(id)
	#ServerSave.update_online_client_file(id)
