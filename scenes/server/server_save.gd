extends Node
### Server func to save and load infos in files

### Update online clients on connection/deconnection NOT USED AT THIS DATE
#func update_online_client_file(new_id: int) -> void :
	#var update_online_clients_directory = DirAccess.open("user://save/server")
	#var update_online_clients = ConfigFile.new()
	#if  update_online_clients_directory.file_exists("user://save/server/online_clients.dat"):
		#### Open and load the file
		#update_online_clients.load("user://save/server/online_clients.dat")
		#var current_onlinecl: Array = update_online_clients.get_value("Online Clients", "online_clients").duplicate(true)
		#if current_onlinecl.has(new_id):
			#current_onlinecl.erase(new_id)
		#else:
			#current_onlinecl.append(new_id)
		#update_online_clients.set_value("Online Clients", "online_clients", current_onlinecl)
	#else:
		### Create the file
		#update_online_clients.set_value("Online Clients", "online_clients", [new_id])
		#### Writing new datas
	#update_online_clients.save("user://save/server/online_clients.dat")
#### Update online players on connection/deconnection


func deleting_player_from_room_group(player_id: int, player_name: String, place_name: String, room_name): ## Update players in group when someone disconnect or change room (Server Side)
	var place_name_ref: String
	var place_name_ref1: String
	### Reformating place_name if needed (adding "_")
	if " " in place_name :
		place_name_ref = place_name.replace(" ", "_")
	else:
		place_name_ref = place_name
	if "'" in place_name_ref :
		place_name_ref1 = place_name_ref.replace("'", "_")
	else:
		place_name_ref1 = place_name_ref
	### Reformating place_name if needed

	var update_place = ConfigFile.new()
	update_place.load("user://save/server/places_player_tracking/"+place_name_ref1+".dat")

	### Deleting player from the file
	var sections:Array = update_place.get_sections()
	for i in sections.size():
		var array_of_player_arrays: Array = update_place.get_value(sections[i], "players").duplicate(true)
		for x in update_place.get_value(sections[i], "players").size():
			#var array_of_player_arrays: Array = update_place.get_value(sections[i], "players").duplicate(true)
			if update_place.get_value(sections[i], "players")[x][1] == player_id :
				array_of_player_arrays.remove_at(x)
		update_place.set_value(sections[i], "players", array_of_player_arrays)

	### Writing new datas
	update_place.save("user://save/server/places_player_tracking/"+place_name_ref1+".dat")

	return(update_place)


func deleting_player_from_online_players(player_name: String) -> void :
	var update_online_players_dir = DirAccess.open("user://save/server")
	if  update_online_players_dir.file_exists(global.online_players_file_path):
		## Open and load the file
		var update_online_players = ConfigFile.new()
		update_online_players.load(global.online_players_file_path)
		print("del_online "+str(update_online_players.get_section_keys("current_online")))
		update_online_players.erase_section_key("current_online", player_name) # Erase player leaving section
		update_online_players.save(global.online_players_file_path)


func make_unique_character_id(player_name: String, player_id: int) -> String : ## Func to store character unique id on the server (from player_save.gd)
	var players_cui = ConfigFile.new()
	#var dir = DirAccess.open("user://save/server")
	var new_character_unique_id: String = str(Time.get_time_string_from_system(false)+player_name+OS.get_locale()+OS.get_unique_id()).sha256_text()
	#if dir.file_exists("user://save/server/players_cui.dat"): ## If the file already exists
		#players_cui.load("user://save/server/players_cui.dat")
		#var temp_dict: Dictionary = players_cui.get_value("players_cui", "dictionary").duplicate(true)
		#temp_dict[player_name] = new_character_unique_id
		#players_cui.set_value("players_cui", "dictionary", temp_dict)
	#else: ## Or create the file
		#players_cui.set_value("players_cui", "dictionary", {
			#player_name: new_character_unique_id
		#})
	players_cui.set_value("players_cuis", new_character_unique_id, player_id)
	players_cui.save(global.players_cuis_file_path)
	return(new_character_unique_id)


func check_character_unique_id(character_unique_id: String)-> bool: ## Server checks if the character_unique_id is matching player_name
	var players_cui = ConfigFile.new()
	players_cui.load(global.players_cuis_file_path)
	if players_cui.get_section_keys("player_cui").has(character_unique_id):
		return(true)
	else:
		return(false)


func save_new_player_multiplayer_id():
	pass


func get_player_multiplayer_id(_character_unique_id):
	pass


func check_online_presence_of_player_in_fight(player_name) -> bool :
	var online_players = ConfigFile.new()
	online_players.load(global.online_players_file_path)
	if online_players.has_section_key("current_online", player_name):
		return(true)
	else:
		return(false)


func update_player_id_for_players_in_combat(player_name: String, new_player_id: int, place: String, room: String): ## Updating players_fighting.dat with new player's multiplayer id (from login.gd)
	var fighting_directory = DirAccess.open("user://save/server")
	if fighting_directory.file_exists(global.players_fighting_file_path):
		var current_fighting = ConfigFile.new()
		current_fighting.load(global.players_fighting_file_path)
		var fighters: Dictionary = current_fighting.get_value(place, room)
		for i in fighters.keys().size():
			if fighters.keys()[i] != "counter" and fighters.keys()[i] != "confront_mode":
				var side: String = fighters.keys()[i]
				for y in fighters[side].keys().size():
					if fighters[side].keys()[y] == player_name:
						var player: String = fighters[side].keys()[y]
						var new_fighters = fighters.duplicate(true)
						new_fighters[side][player].erase("id")
						new_fighters[side][player_name]["id"] = new_player_id
						current_fighting.set_value(place, room, new_fighters)
				current_fighting.save(global.players_fighting_file_path)


#####################################################
### Rework to do to the file formating for room chats
#####################################################
func _room_chat_in_file(place_name: String, room_name: String, new_text: String) -> void : ## Func to save the live chat in an array in a file (every room have is own file)
	var saving_chat_text = ConfigFile.new()
	var chat_dir = DirAccess.open("user://save/server/rooms_chat")
	if chat_dir.file_exists("user://save/server/rooms_chat/"+place_name+"_"+room_name+".dat"):
		## Open and load the file
		saving_chat_text.load("user://save/server/rooms_chat/"+place_name+"_"+room_name+".dat")

		var old_chat = saving_chat_text.get_value(place_name, room_name)
		old_chat.append(new_text)
		saving_chat_text.set_value(place_name, room_name, [new_text])

	else:
		## Create the file
		saving_chat_text.set_value(place_name, room_name, [new_text])

	## Writing new datas
	saving_chat_text.save("user://save/server/rooms_chat/"+place_name+"_"+room_name+".dat")

#####################################################
### Rework to do to the file formating for room chats
#####################################################

func _server_data_save(_server_log_entry) -> void : pass
	#var date = Time.get_datetime_dict_from_system()
	#var data_server = ConfigFile.new()
	#if data_server.file_exists("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat") == true  : 
		#data_server.load("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat")
		#if data_server.has_section_keys(str(date.year), str(date.month)):
			#var old_value = data_server.get_value(str(date.year), str(date.month))
			#data_server.set_value(str(date.year), str(date.month), old_value+[str(date.day), server_log_entry])
		#else:
			#data_server.set_value(str(date.year), str(date.month), [str(date.day), server_log_entry])
#
	#else :
		#data_server.set_value(str(date.year), str(date.month), [str(date.day), server_log_entry])
	#data_server.save("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat")
