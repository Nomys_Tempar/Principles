extends Node
### Connexion to server scene or server creation scene

const login = preload("res://scenes/server/login.tscn")
const game_intro = preload("res://scenes/game_intro/intro_game.tscn")
const exit_and_option_scene = preload("res://scenes/menus/escape_scene.tscn")

func _on_configure_server_pressed() -> void :
	global.server = 1

	### Creating necessary folders and auto-connecting server
	var directory = DirAccess.open("user://")
	if directory.dir_exists("save") != true :
		directory.make_dir("save")
	if directory.dir_exists("user://save/server") != true :
		directory.make_dir("user://save/server")
	if directory.dir_exists("user://save/server/places_player_tracking") != true:
		directory.make_dir("user://save/server/places_player_tracking")
	if directory.dir_exists("user://save/server/players") != true:
		directory.make_dir("user://save/server/players")
	if directory.dir_exists("user://save/server/places_logs") != true:
		directory.make_dir("user://save/server/places_logs")

	get_node("network").server_valid()


func _on_connect_pressed() -> void :
	### Creating necessary folders and checking server
	var directory = DirAccess.open("user://")
	if directory.dir_exists("save") != true :
		directory.make_dir("save")
	if directory.dir_exists("user://save/player") != true :
		directory.make_dir("user://save/player")

	get_node("network").server_valid()


func start_login_and_intro() -> void :
	var login_scene = login.instantiate()
	add_child(login_scene)
	var game_intro_scene = game_intro.instantiate()
	add_child(game_intro_scene)


### exit and option menu trigger 
func _unhandled_input(event) -> void :
	if global.server == 0:
		if event is InputEvent and event is InputEventKey:
			if event.pressed and event.keycode == KEY_ESCAPE and has_node("escape_scene") == false :
					var option_and_exit_menu = exit_and_option_scene.instantiate()
					add_child(option_and_exit_menu)
					get_node("./escape_scene/CenterContainer/VBoxContainer/exit").connect("pressed",Callable(self,"_exit_is_pressed"))
					get_node("./escape_scene/CenterContainer/VBoxContainer/disconnect").connect("pressed",Callable(self,"_disconnect_is_pressed"))
			elif event.pressed and event.keycode == KEY_ESCAPE and has_node("escape_scene") :
				get_node("./escape_scene").free()
### exit and option menu trigger 


func _disconnect_is_pressed() -> void :
	if multiplayer.get_unique_id() != 1:
		if PlayerData.player_name != "server":
			PlayerSave.savegame()
		rpc_id(1, "_disconnecting_or_leaving_place", PlayerData.player_name, PlayerData.player_current_place, PlayerData.player_current_room) # Player notify the server when disconnecting or leaving a place	
	get_node("login").queue_free()
	global.login_done = false


func _exit_is_pressed() -> void :
	if multiplayer.get_unique_id() != 1:
		if PlayerData.player_name != "server":
			PlayerSave.savegame()
		rpc_id(1, "_disconnecting_or_leaving_place", PlayerData.player_name, PlayerData.player_current_place, PlayerData.player_current_room) # Player notify the server when disconnecting or leaving a place	
	get_tree().quit()


@rpc("any_peer", "call_remote", "reliable")
func _disconnecting_or_leaving_place(player_name: String, place_name: String, room_name) -> void : # Server receiving disconnect player's notification
	if player_name != "server":
		var player_id = multiplayer.get_remote_sender_id()
		ServerSave.deleting_player_from_room_group(player_id, player_name, place_name, room_name)
		ServerSave.deleting_player_from_online_players(player_name)


#func _enter_tree() -> void : # Deleting files on exit for debugging purpose
	#if multiplayer.get_unique_id() == 1 and global.dev == true:
		#print("del")
		#var _dir = DirAccess.open("user://save/server")
#		dir.remove("user://save/server/online_clients.dat")
		#dir.remove("user://save/server/online_players.dat")
#		dir.remove("user://save/server/places_player_tracking/place_two.dat")
		#dir.remove("user://save/server/places_player_tracking/start_place.dat")
		#dir.remove("user://save/server/player_fighting.dat")
