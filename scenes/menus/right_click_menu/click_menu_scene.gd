extends Node

### Right click menu trigger by "clickable.gd"

var click_possible:bool ## PopupMenu possible
var click_on_popup:bool ## Click on PopupMenu possible
var node_on_click
var parent_path
var text: String
var other_id: int


func _input(event) -> void :
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_RIGHT and event.pressed:
			if click_possible == true : ## Display the PopupMenu and items
					get_node("PopupMenu").popup()
					get_node("PopupMenu").set_position(event.position)
					if node_on_click.get_name() == "live_container": ## Setting the save item for saving livefeed
						get_node("PopupMenu").clear()
						get_node("PopupMenu").add_item("Save")
			elif click_on_popup == false:
				if get_node("PopupMenu").visible == true :
					get_node("PopupMenu").clear()
					get_node("PopupMenu").visible = false


func on_clickable(node_parent, text_to_use: String) -> void : ## Livefeed labels activated
	click_possible = true
	node_on_click = node_parent
	text = text_to_use


func not_on_clickable() -> void : ## Livefeed labels desactivated
	click_possible = false


func itemlist_clicked(node_parent, other_name: String, item_position: Vector2, mouse_button: int, id: int) -> void : ## Room itemlist
	if mouse_button == 2: ## Right click button
		node_on_click = node_parent
		text = other_name
		other_id = id
		get_node("PopupMenu").popup()
		get_node("PopupMenu").set_position(node_parent.get_global_position()+item_position)
		get_node("PopupMenu").clear()
		get_node("PopupMenu").add_item("Description")


func _on_popup_menu_mouse_entered() -> void : ## Tracking of the mouse on the PopupMenu
	click_on_popup = true


func _on_popup_menu_mouse_exited() -> void : ## Tracking of the mouse on the PopupMenu
	click_on_popup = false


func _on_popup_menu_index_pressed(id: int) -> void :
	if id == 0:
		if node_on_click.get_name() == "live_container":
			get_node("item_savefeed").savefeed(text)
		elif node_on_click.get_name().contains("list_loc"):
			get_node("rooms_handling").get_character_name(text, other_id)
