extends Node

var date:Dictionary = Time.get_datetime_dict_from_system()

func savefeed(saved_text: String) -> void :
	PlayerData.dialog_saved = "-- Day "+str(date.day)+" Month "+str(date.month)+" Year "+str(date.year)+"\n"+saved_text+"\n"
	PlayerSave.savegame()
	PlayerData.dialog_saved = null
