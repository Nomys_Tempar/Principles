extends Control
### Script to give a node a right click menu (see "click_menu_scene")

func _on_mouse_entered() -> void : ## Activate click_menu_scene
	#if get_node(".") is ItemList : ## On rooms
		#get_node("/root/connection/login/pj/click_menu_scene").on_clickable(get_node("."), "text")
	#else: ## On Livefeed Labels
	get_node("/root/connection/login/pj/click_menu_scene").on_clickable(get_parent(), get_node(".").get_text())


func _on_mouse_exited() -> void : ## Desactivate click_menu_scene
	if get_node(".") is ItemList : ## On rooms
		get_node(".").deselect_all()
		#get_node("/root/connection/login/pj/click_menu_scene").not_on_clickable()
	else: ## On Livefeed Labels
		get_node("/root/connection/login/pj/click_menu_scene").not_on_clickable()


func _on_item_clicked(index, at_position, mouse_button_index):
	get_node("/root/connection/login/pj/click_menu_scene").itemlist_clicked(get_node("."), get_node(".").get_item_text(index), at_position, mouse_button_index, get_node(".").get_item_metadata(index))
