extends Control

const option_scene = preload("res://scenes/menus/options.tscn")

func _ready() -> void :
	get_node("CenterContainer/VBoxContainer/options").set_text("Options")
	get_node("CenterContainer/VBoxContainer/disconnect").set_text("Disconnect")
	get_node("CenterContainer/VBoxContainer/exit").set_text("Exit")
	#if global.login_done == false :
		#get_node("CenterContainer/VBoxContainer/disconnect").visible = false


func _on_options_pressed() -> void :
	var options = option_scene.instantiate()
	add_child(options)


func _on_exit_pressed() -> void :
	pass


func _on_disconnect_pressed() -> void :
	queue_free()
