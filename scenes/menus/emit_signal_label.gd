extends Label # Script to make a node send a signal when hovered

signal node_name(name)

func _ready() -> void :
	set_mouse_filter(MOUSE_FILTER_STOP)


func _on_mouse_entered() -> void :
	emit_signal("node_name", get_name())
