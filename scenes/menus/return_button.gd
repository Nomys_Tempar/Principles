extends Button

var help:bool = false

func _ready() -> void :
	connect("button_up",Callable(self,"_on_pressed"))
	_set_text()
	#_theme_selection()


func _theme_selection() -> void :
	var theme_
	set_theme(theme_)


func _on_pressed() -> void :
#	owner.get_node("button_back").on_pressed() # Sound trigger
#
#	await get_tree().create_timer(0.14).timeout
#
#	if (global.ingame == true or global.maker_section == true) and help == false :
#		get_node("/root/connection_back/main_menu")._theme_selection()
#		get_node("/root/connection_back/main_menu/loading").queue_free()
#
#	elif global.maker_section == true and help == true :
#		get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/maker_text_scene/maker_help").queue_free()
#	elif global.current_scene == "logs":
#		owner.queue_free()
#		global.current_scene = "main_menu"
#	elif global.current_scene == "options_conn" :
#		get_node("/root/connection_back")._back_from_option()
#		owner.queue_free()
#		global.current_scene = "connection"
#	elif global.current_scene == "options_main" :
#		get_node("/root/connection_back/main_menu")._back_from_option()
#		owner.queue_free()
#		global.current_scene = "main_menu"
#	elif global.current_scene == "credits":
#		owner.queue_free()
#		global.current_scene = "main_menu"
#	elif global.current_scene == "makerz_charts":
#		owner.queue_free()
#		global.current_scene = "main_menu"
#	else :
#		owner.get_node("bgControl").queue_free()
#		get_node("/root/connection_back/main_menu")._theme_selection()

	if get_name() == "back":
		owner.queue_free()


func _help() -> void :
	$".".position = Vector2(370, 325)
	help = true


func _set_text() -> void :
#	if global.maker_section == true && help == false :
#		set_text("Quit")
#	elif global.maker_section == true && help == true :
#		set_text("Close")
#	else :
	set_text("Back")
