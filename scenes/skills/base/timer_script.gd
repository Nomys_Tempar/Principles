extends Timer ## Script assign to a temp confrontation timer Server-side

func ready_func() -> void :
	connect("timeout", Callable(self,"_counter_ends")) ## Connect signal for the confrontation counter


func _counter_ends() -> void : ## Server func to launch turn resolution when the timer server-side ends
	get_parent().counter_ends(get_meta("place"), get_meta("room"))


func _exit_tree() -> void:
	get_parent().erase_terminated_timer(get_name())
