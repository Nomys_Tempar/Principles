extends Control

var place_button

func _ready() -> void :
	_setup_places_buttons()


func _setup_places_buttons() -> void :
	place_button = Button.new()
	$"./cc_background/CenterContainer/GridContainer".add_child(place_button)
	get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).set_name(PlayerData.player_current_place)
	get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).set_text(PlayerData.player_current_place)
	get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).disabled = true

	for i in global.place_leads_to.size():
		if "[hidden]" in global.place_leads_to[i]:
			if PlayerData.player_known_places.has(global.place_leads_to[i]): ## If player know the hidden place connected to the current place, we create the button to travel to it
				place_button = Button.new()
				$"./cc_background/CenterContainer/GridContainer".add_child(place_button)
				get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).set_name(global.place_leads_to[i].erase(0, 8))
				get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).set_text(global.place_leads_to[i].erase(0, 8))
				get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).connect("pressed",Callable(self,"_travel").bind(get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).get_name()))
		else:
			place_button = Button.new()
			$"./cc_background/CenterContainer/GridContainer".add_child(place_button)
			get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).set_name(global.place_leads_to[i])
			get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).set_text(global.place_leads_to[i])
			get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).connect("pressed",Callable(self,"_travel").bind(get_node("cc_background/CenterContainer/GridContainer").get_child($"./cc_background/CenterContainer/GridContainer".get_child_count()-1).get_name()))


func _travel(new_place) -> void :
	get_parent().get_node("main_interface/HSplitContainer/place_panel/place_main/vplace_title/vplace/place_infos").change_room("leave")
	await get_parent().get_node("main_interface/HSplitContainer/place_panel/place_main/vplace_title/vplace/place_infos").room_cleaned
	get_parent().get_node("main_interface/HSplitContainer/place_panel/place_main").change_place(new_place)
	queue_free()


func _exit_tree():
	get_node("/root/connection/login/pj").action_done()
