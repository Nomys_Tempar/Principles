extends Control

var actions_selected:Array = []
var result_primary # Server only var
var result_secondary # Server only var
var primtext_final:String
var secondtext_final:String
var store_fight_data:Array = [] ## Server array of arrays to store [player_name, place_room, actions_selected, stats_concat] for a player when an action is changed
var store_fight_results: Array = [] ## Server array of arrays to store [place_room, [player_name, result_primary, result_secondary]
var players_on_hold: Array = [] ## Server array of arrays of players offline waiting for a turn resolution : [place_room, player_name]
var players_end: int = 0 ## Int to check how many players have finish processing the round to end the fight

signal wound(result_primary, wound_type)


func _ready():
	if global.server != 1:
		if PlayerData.in_combat == false : ## The confrontation is starting
			PlayerData.in_combat = true
		else : ## Player reconnect onto an ongoing confrontation
			print("return to confrontation")
			var place_room:Array = [PlayerData.player_current_place, PlayerData.player_current_room] ## Array of place and room
			rpc_id(1, "_asking_for_results", PlayerData.player_name, place_room)
		%valid_actions.disabled = false
		PlayerSave.savegame() # Saving player states
		_setup_skills()
		_setup_opponents()
		connect("wound", Callable(get_tree().get_nodes_in_group("player")[1],"wound")) ## Connect combat wound signal to lifebar wound function
		_format_and_saving_player_actions_on_server() ## Initial actions save on server


func _setup_opponents() -> void :
	## List players involved in combat
	for i in PlayerData.opponents.size(): # List opponents in dedicated itemlist
		%opponent_list.add_item(PlayerData.opponents[i][0])
	%player_list.add_item(PlayerData.player_name) # List player in dedicated itemlist
	# TODO add allies in player_list node


### Actions setup handling
func _setup_skills() -> void :
	%primary_action.add_item("Do Nothing")
	%primary_action.set_item_metadata(%primary_action.get_item_count()-1, "nothing")
	%secondary_action.add_item("Do Nothing")
	%secondary_action.set_item_metadata(%secondary_action.get_item_count()-1, "nothing")

	var skills_prim_dir = DirAccess.open("res://game_data/skills/confrontation/primaries/")
	var skills_second_dir = DirAccess.open("res://game_data/skills/confrontation/secondaries/")
	for i in skills_prim_dir.get_files().size(): # Setting up primary skills
		var new_skill = ConfigFile.new()
		new_skill.load("res://game_data/skills/confrontation/primaries/"+skills_prim_dir.get_files()[i])
		var section = new_skill.get_sections()
		if new_skill.get_value(section[0], "confront_mode") == PlayerData.confrontation_mode or new_skill.get_value(section[0], "confront_mode") == -1 : # Checking skills availability regarding confrontation_mode
			%primary_action.add_item(new_skill.get_value(section[0], "name"))
			%primary_action.set_item_metadata(%primary_action.get_item_count()-1, skills_prim_dir.get_files()[i])
	for i in skills_second_dir.get_files().size(): # Setting up secondary skills
		var new_skill = ConfigFile.new()
		new_skill.load("res://game_data/skills/confrontation/secondaries/"+skills_second_dir.get_files()[i])
		var section = new_skill.get_sections()
		if new_skill.get_value(section[0], "confront_mode") == PlayerData.confrontation_mode or new_skill.get_value(section[0], "confront_mode") == -1 : # Checking skills availability regarding confrontation_mode
			%secondary_action.add_item(new_skill.get_value(section[0], "name"))
			%secondary_action.set_item_metadata(%secondary_action.get_item_count()-1, skills_second_dir.get_files()[i])
	%primary_action.select(-1)
	%secondary_action.select(-1)


func _on_primary_action_item_selected(index) -> void :
	actions_selected.pop_front()
	actions_selected.push_front(%primary_action.get_item_metadata(index))
	## Setup skill description
	if %primary_action.get_item_text(index) == "Do Nothing" :
		%prim_descr.set_text("")
	else:
		var skill = ConfigFile.new()
		skill.load("res://game_data/skills/confrontation/primaries/"+%primary_action.get_item_metadata(index))
		%prim_descr.set_text(skill.get_value(skill.get_sections()[0], "description"))

	_format_and_saving_player_actions_on_server()


func _on_secondary_action_item_selected(index) -> void :
	actions_selected.pop_back()
	actions_selected.push_back(%secondary_action.get_item_metadata(index))
	## Setup skill description
	if %secondary_action.get_item_text(index) == "Do Nothing" :
		%second_descr.set_text("")
	else:
		var skill = ConfigFile.new()
		skill.load("res://game_data/skills/confrontation/secondaries/"+%secondary_action.get_item_metadata(index))
		%second_descr.set_text(skill.get_value(skill.get_sections()[0], "description"))

	_format_and_saving_player_actions_on_server()


### Saving player actions functions
func _format_and_saving_player_actions_on_server(): ## Formating players actions and sending to the Server
	if actions_selected.is_empty() == true : ## Checking of actions_selected content to remove null entries
		actions_selected.push_front("nothing")
		actions_selected.push_back("nothing")
	else:
		for action_position in actions_selected.size(): ## Checking of actions_selected content to remove null entries
			if actions_selected[action_position] == null:
				actions_selected.remove_at(action_position)
				if action_position == 0:
					actions_selected.push_front("nothing")
				else:
					actions_selected.push_back("nothing")
	var place_room:Array = [PlayerData.player_current_place, PlayerData.player_current_room] ## Getting place and room
	rpc_id(1, "_saving_fight_data_on_server",  PlayerData.player_name, place_room, actions_selected, PlayerData.stats_concat)


@rpc("any_peer", "call_remote")
func _saving_fight_data_on_server(player_name: String, place_room: Array, new_actions_selected: Array, player_stats_concat: Dictionary) -> void :
	var new_array: Array = store_fight_data.duplicate(true)
	if store_fight_data.is_empty() == false :
		for i in store_fight_data.size():
			if store_fight_data[i][0] == player_name :
				new_array.remove_at(i)
	new_array.append([player_name,place_room, new_actions_selected, player_stats_concat])
	store_fight_data = new_array


### Launching turn functions
func _on_valid_actions_pressed() -> void :
	%valid_actions.disabled = true
	if actions_selected.size() <= 1:
		actions_selected = [%primary_action.get_selected_metadata(), %secondary_action.get_selected_metadata()]
	var place_room:Array = [PlayerData.player_current_place, PlayerData.player_current_room] ## Getting place and room
	_format_and_saving_player_actions_on_server()
	await get_tree().create_timer(0.5).timeout
	rpc_id(1, "_ready_to_fight", PlayerData.player_name, PlayerData.stats_concat, place_room)


@rpc("any_peer", "call_remote")
func _ready_to_fight(player_name: String, player_stats: Dictionary, place_room: Array): ## Server func to check and store data of a player in fight is ready to launch the round
	var player_is_in_fight: bool = false
	## Verify player fighting state
	var current_fighting = ConfigFile.new()
	current_fighting.load(global.players_fighting_file_path)
	var fighting: Dictionary = current_fighting.get_value(place_room[0], place_room[1])
	for i in fighting.keys().size(): ## If player is in fighting dict...
		if fighting.keys()[i] != "counter" and fighting.keys()[i] != "confront_mode":
			var sides: String = fighting.keys()[i]
			for y in fighting[sides].size():
				if fighting[sides].keys()[y] == player_name :
					player_is_in_fight = true
	if player_is_in_fight == true : ## Checking if the player other is ready or not
		var players_ready_numb: int = 0
		for i in fighting.keys().size():
			if fighting.keys()[i] != "counter" and fighting.keys()[i] != "confront_mode":
				var sides: String = fighting.keys()[i]
				for y in fighting[sides].size():
					var players_in_sides: String = fighting[sides].keys()[y]
					if fighting[sides][players_in_sides]["ready"] == true :
						players_ready_numb += 1
		if players_ready_numb == 0: ## No one is ready, we make the player ready
			var new_fighting_dict: Dictionary = fighting.duplicate(true)
			for x in new_fighting_dict.keys().size():
				if new_fighting_dict.keys()[x] != "counter" and fighting.keys()[x] != "confront_mode":
					var new_sides: String = new_fighting_dict.keys()[x]
					for name_index in new_fighting_dict[new_sides].keys().size():
						if new_fighting_dict[new_sides].keys()[name_index] == player_name :
							new_fighting_dict[new_sides][player_name].erase("ready")
							new_fighting_dict[new_sides][player_name]["ready"] = true
							current_fighting.set_value(place_room[0], place_room[1], new_fighting_dict)
							current_fighting.save(global.players_fighting_file_path)
		else: ## Opponent is ready, so we can launch the round
			%round_launch.end_timer(place_room)
			_server_process_round(place_room)


func on_timer_timeout(place, room) : ## Server func to launch the round on the fight timer end (see combat_timer.gd)
	var place_room:Array = [place, room] ## Array of place and room
	_server_process_round(place_room)


###Server confrontation functions
@rpc("any_peer")
func _server_process_round(place_room:Array) -> void :
	var current_fight = ConfigFile.new()
	current_fight.load(global.players_fighting_file_path)
	var fight_dict: Dictionary = current_fight.get_value(place_room[0], place_room[1])
	_resolve_on_server(place_room, fight_dict)


func _resolve_on_server(place_room:Array, fight_dict: Dictionary) -> void :
	var player_actions: Array
	var player_stats: Dictionary
	var opponent_stats: Dictionary
	var players_in_fight: Array ## Player names in current confrontation

	## Checking store_fight_results content and adding place_room var
	var new_array: Array = store_fight_results.duplicate(true)
	if store_fight_results.is_empty() == false:
		for i in store_fight_results.size():
			if store_fight_results[i][0] == place_room :
				new_array.remove_at(i)
	new_array.append([place_room])
	store_fight_results = new_array

	for i in fight_dict.keys().size(): ## Storing all player_names in fight into players_in_fight var
		if fight_dict.keys()[i] != "counter" and fight_dict.keys()[i] != "confront_mode":
			var sides: String = fight_dict.keys()[i]
			for y in fight_dict[sides].keys().size():
				var players: String = fight_dict[sides].keys()[y]
				players_in_fight.append(players)

	for i in players_in_fight.size():
		var player_name: String
		for y in fight_dict.keys().size(): ## Setting up player_name, player_actions, player_stats and opponent_stats var
			if fight_dict.keys()[y] != "counter" and fight_dict.keys()[y] != "confront_mode":
				var sides: String = fight_dict.keys()[y]
				for u in fight_dict[sides].keys().size():
					var players: String = fight_dict[sides].keys()[u]
					if players == players_in_fight[i] : ## Main player
						player_name = fight_dict[sides].keys()[0]
						player_stats = fight_dict[sides][player_name]["stats"]
						for x in store_fight_data.size():
							if store_fight_data[x][0] == player_name :
								player_actions = store_fight_data[x][2]
					else: ## Opponent
						opponent_stats = fight_dict[sides][players]["stats"]
		## Resolving confrontation
		_resolve_primary(player_actions[0], player_stats, opponent_stats)
		_resolve_secondary(player_actions[1], player_stats, opponent_stats)

		## Adding results in store_fight_results var
		for x in store_fight_results.size():
			if store_fight_results[x][0] == place_room :
				store_fight_results[x].append([player_name, result_primary, result_secondary])

	## Asking for results and handling of online and offline players
	for p in players_in_fight.size():
		if ServerSave.check_online_presence_of_player_in_fight(players_in_fight[p]) == true : ## If the player is online server send the results directly
			players_on_hold.append([place_room, players_in_fight[p]]) ## Adding player to the array of players waiting for results
			print("server_asking")
			_asking_for_results(players_in_fight[p], place_room)
		else:
			players_on_hold.append([place_room, players_in_fight[p]]) ## Adding player to the array of players waiting for results


func _resolve_primary(primary:String, player_stats:Dictionary, opponent_stats:Dictionary): # Server func to resolve primary action
	if primary != "nothing" && primary != null: # If the player used an action
		## Getting the skill used
		var prim_skill = ConfigFile.new()
		prim_skill.load("res://game_data/skills/confrontation/primaries/"+primary)
		var sections:Array = prim_skill.get_sections()
		var stat_to_confront = prim_skill.get_value(sections[0], "stat_to_confront")
		var stat_bonus_damage = prim_skill.get_value(sections[0], "stat_bonus_damage")
		if int(player_stats[stat_to_confront]) >= int(opponent_stats[stat_to_confront]):
			result_primary = (int(player_stats[stat_bonus_damage])%10) + (prim_skill.get_value(sections[0], "base_damage")-int(opponent_stats[stat_bonus_damage]))
			if result_primary <= 0 :
				result_primary = "failure"
		else:
			result_primary = "failure"
	else:
		result_primary = null
	return(result_primary)


func _resolve_secondary(secondary:String, player_stats:Dictionary, opponent_stats:Dictionary): # Server func to resolve secondary action
	if secondary != "nothing" && secondary != null : # If the player used an action
		## Getting the skill used
		var second_skill = ConfigFile.new()
		second_skill.load("res://game_data/skills/confrontation/secondaries/"+secondary)
		var sections:Array = second_skill.get_sections()
		if second_skill.get_value(sections[0], "confront_mode") == 0 : # If physical
			var stat_to_confront = second_skill.get_value(sections[0],"stat_to_confront")
			if (int(player_stats[stat_to_confront])+second_skill.get_value(sections[0],"stat_bonus")) > int(opponent_stats[stat_to_confront]):
				result_secondary = second_skill.get_value(sections[0],"effect")
			else:
				result_secondary = "failure"
		elif second_skill.get_value(sections[0], "confront_mode") == -1 : # If basic skill
			var stat_to_confront = second_skill.get_value(sections[0],"stat_to_confront")
			if int(player_stats[stat_to_confront]) > int(opponent_stats[stat_to_confront]):
				result_secondary = second_skill.get_value(sections[0],"effect")
			else:
				result_secondary = "failure"
	else: 
		result_secondary = null
	return(result_secondary)


@rpc("any_peer", "call_remote")
func _asking_for_results(player_name: String, place_room: Array): ## Server func to get results (trigger by players on connection if in fight or by the sever if both players are online) 
	print("asking")
	for i in store_fight_results.size():
		if store_fight_results[i][0] == place_room :
			var current_fight = ConfigFile.new()
			current_fight.load(global.players_fighting_file_path)
			var fight_dict: Dictionary = current_fight.get_value(place_room[0], place_room[1])
			for x in fight_dict.keys().size():
				if fight_dict.keys()[x] != "counter" and fight_dict.keys()[x] != "confront_mode":
					var sides: String = fight_dict.keys()[x]
					for u in fight_dict[sides].keys().size():
						if fight_dict[sides].keys()[u] == player_name :
							#print(store_fight_results[i])
							var confrontation_results: Array = store_fight_results[i].duplicate(true)
							confrontation_results.pop_front()
							var confrontation_actions: Array = []
							for k in store_fight_data.size():
								if store_fight_data[k][1] == place_room :
									confrontation_actions.append([store_fight_data[k][0], store_fight_data[k][2]])
							## Sending results to player
							rpc_id(fight_dict[sides][player_name]["id"], "_damage_done", confrontation_results, confrontation_actions)

							## Cleaning if all players in confrontation have their results
							var all_done: int = 0
							if players_on_hold.has([place_room, player_name]) == true :
								for o in players_on_hold.size():
									if players_on_hold[o][0] == place_room :
										all_done += 1
							if all_done == 2 :
								print("all_done")
								_server_clean_round(place_room, player_name) ## Cleaning round function


func _server_clean_round(place_room: Array, player_name: String): ## Server func to clean round data arrays and ready state in player_fighting.dat
	## Cleaning arrays
	var data_indexes: Array ## Two int to locate round data inside store_fight_data array
	for z in store_fight_data.size() :
		if store_fight_data[z][1] == place_room :
			data_indexes.append(z)
	for i in data_indexes.size():
		store_fight_data.remove_at(data_indexes[i-1])
	var new_results: Array ## Cleaning store_fight_results
	for z in store_fight_results.size():
		if store_fight_results[z][0] == place_room :
			new_results = store_fight_results.duplicate(true)
			new_results.remove_at(z)
	store_fight_results = new_results
	var hold_indexes: Array ## Two int to locate on hold players arrays in players_on_hold
	for z in players_on_hold.size():
		if players_on_hold[z][0] == place_room :
			hold_indexes.append(z)
	for i in hold_indexes.size():
		players_on_hold.remove_at(hold_indexes[i-1])
		
	## Cleaning player ready state
	var current_fight = ConfigFile.new()
	current_fight.load(global.players_fighting_file_path)
	var fight_dict: Dictionary = current_fight.get_value(place_room[0], place_room[1])
	for i in fight_dict.keys().size():
		if fight_dict.keys()[i] != "counter" and fight_dict.keys()[i] != "confront_mode":
			var sides: String = fight_dict.keys()[i]
			for y in fight_dict[sides].keys().size():
				var players: String = fight_dict[sides].keys()[y]
				fight_dict[sides][players].erase("ready")
				fight_dict[sides][players]["ready"] = false
				current_fight.set_value(place_room[0], place_room[1], fight_dict)
				current_fight.save(global.players_fighting_file_path)


### Players handling turn results functions
@rpc("authority", "call_remote")
func _damage_done(results: Array, actions :Array) -> void : ## Primary action results processed on player
	print("damage done")
	print(results)
	var prim_skill = ConfigFile.new()
	for i in results.size():
		if results[i][1] != null:
			for y in actions.size():
				if results[i][0] == actions[y][0]:
					prim_skill.load("res://game_data/skills/confrontation/primaries/"+actions[y][1][0])
					var sections:Array = prim_skill.get_sections()
					## Processing primary skill text
					var prim_text: String
					if results[i][1] is String : ## String is "failure"
						prim_text = prim_skill.get_value(sections[0], "failure_text")
						_refactor_primtext(prim_text, results[i])
						if PlayerData.player_name == actions[y][0] : ## Player failing display the text
							get_tree().get_nodes_in_group("player")[2].on_player_entry_text_submitted(primtext_final, true, "Room") ## Display narration text in player's livefeed
					else: ## Int is success (damage number)
						prim_text = prim_skill.get_value(sections[0], "success_text")
						print(actions[y])
						if PlayerData.player_name != actions[y][0] : ## Player attacked take the damage
							emit_signal("wound", prim_skill.get_value(sections[0], "damage_type"), results[i][1]) ## Sending damage to lifebar.gd
						else: ## Other player display the text
							_refactor_primtext(prim_text, results[i])
							get_tree().get_nodes_in_group("player")[2].on_player_entry_text_submitted(primtext_final, true, "Room") ## Display narration text in player's livefeed
	_effect_done(results, actions) ## Proceed to secondary action


func _refactor_primtext(prim_text: String, results: Array) -> String: ## Looking for [player_name], [opponent_name] and [damage]
	var mod1: String
	if "[player_name]" in prim_text :
		mod1 = prim_text.replace("[player_name]", "[b]"+results[0]+"[/b]")
	else:
		mod1 = prim_text
	var mod2:String
	if "[opponent_name]" in mod1 :
		if results[0] == PlayerData.player_name : ## TO REDO for multiple fighters in each sides
			mod2 = mod1.replace("[opponent_name]", "[b]"+PlayerData.opponents[0][0]+"[/b]")
		else:
			mod2 = mod1.replace("[opponent_name]", "[b]"+PlayerData.player_name+"[/b]")
	else:
		mod2 = mod1
	var mod3: String
	if "[damage]" in mod2 :
		mod3 = mod2.replace("[damage]", "[b]"+str(results[1])+"[/b]")
	else:
		mod3 = mod2
	primtext_final = mod3
	return(primtext_final)


func _effect_done(results: Array, actions :Array) -> void :
	print("effect done")
	var second_skill = ConfigFile.new()
	var end_confrontation: bool = false
	for i in results.size():
		if results[i][2] != null:
			for y in actions.size():
				if results[i][0] == actions[y][0]:
					second_skill.load("res://game_data/skills/confrontation/secondaries/"+actions[y][1][1])
					var sections:Array = second_skill.get_sections()
					## Processing secondary skill text
					var second_text: String
					if results[i][2] is String : # String is "failure"
						second_text = second_skill.get_value(sections[0], "failure_text")
					else: ## Array is sucess
						second_text = second_skill.get_value(sections[0], "success_text")
						if results[i][2][2] == "global" : ## Effect apply to all player localy
							print("global")
							PlayerData[results[i][2][0]] = results[i][2][1]
						else: ## Local effect
							print("local")
							if results[i][2][0] == "leave confrontation" : ## Build-in : Flee confrontation
								print("fleeing")
								#_fleeing(second_text)
								end_confrontation = true
					if PlayerData.player_name == actions[y][0] : ## Player doing the effect display the text
						_refactor_secondtext(second_text, results[i])
						get_tree().get_nodes_in_group("player")[2].on_player_entry_text_submitted(secondtext_final, true, "Room") ## Display narration text in player's livefeed
	_end_turn(end_confrontation)


func _refactor_secondtext(second_text: String, results: Array) -> String : ## Looking for [player_name], [opponent_name] and [damage]
	var mod1: String
	if "[player_name]" in second_text:
		mod1 = second_text.replace("[player_name]", "[b]"+results[0]+"[/b]")
	else:
		mod1 = second_text
	var mod2:String
	if "[opponent_name]" in mod1:
		if results[0] == PlayerData.player_name : ## TO REDO for multiple fighters in each sides
			mod2 = mod1.replace("[opponent_name]", "[b]"+PlayerData.opponents[0][0]+"[/b]")
		else:
			mod2 = mod1.replace("[opponent_name]", "[b]"+PlayerData.player_name+"[/b]")
	else:
		mod2 = mod1
	secondtext_final = mod2
	return(secondtext_final)


func _end_turn(end_confrontation: bool) -> void :
	print(PlayerData.player_name+" end_combat_turn")
	print(end_confrontation)
	if end_confrontation == false :
		%primary_action.clear() ## Clear primary action button
		%secondary_action.clear() ## Clear secondary action button
		_setup_skills() ## Setup skills for the next turn
		actions_selected.clear() ## Reset player actions
		%valid_actions.disabled = false
		%primary_action.select(-1)
		%secondary_action.select(-1)
		_format_and_saving_player_actions_on_server() ## Initial actions save on server
	else: ## end_confrontation is true
		if PlayerData.player_unconscious == true :
			print("unconscious")
			get_tree().get_nodes_in_group("player")[2].on_player_entry_text_submitted(PlayerData.player_name+" is unconscious!", true, "Room") ## Display narration text in player's livefeed
		if PlayerData.player_dead == true : 
			print("dead") ## Player dead scene
		rpc_id(1, "_confrontation_over", PlayerData.player_current_place, PlayerData.player_current_room, PlayerData.opponents)
	PlayerSave.savegame() # Saving player states


@rpc("any_peer", "call_remote")
func _confrontation_over(place: String, room: String, opponents: Array) -> void: ## Server func to end and clear datas from a confrontation
	players_end += 1 ## TODO clean to avoid bugs on multiples confrontations ending in the same time
	if players_end == opponents.size()+1:
		players_end = 0
		print("confront_over")
		%round_launch.get_node(place+room).queue_free() ## Deleting confrontation timer
		get_parent().confrontation_is_over(place, room) ## Deleting confrontation in ConfigFile in combat_management.gd
