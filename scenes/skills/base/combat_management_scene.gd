extends Control

const combat = preload("res://scenes/skills/base/combat_scene.tscn")
var opponent:String

func _ready() -> void :
	if PlayerData.in_combat == true or multiplayer.is_server() == true : # If the character was waged by another
		if multiplayer.is_server() == false :
			rpc_id(1, "_give_stats_to_server", PlayerData.player_name, PlayerData.stats_concat, PlayerData.player_current_place, PlayerData.player_current_room)
		get_node("ec_background").visible = false
		var combat_scene = combat.instantiate()
		add_child(combat_scene)
	else:
		get_node("ec_background/VBoxContainer/ok").connect("pressed",Callable(self,"_valid"))
		get_node("ec_background/VBoxContainer/ItemList").connect("item_selected",Callable(self,"_opponent_selected"))
		print(PlayerData.player_place_group)
		for i in PlayerData.player_place_group.size() :
			if PlayerData.player_place_group[i][0] == PlayerData.player_current_room :
				for y in PlayerData.player_place_group[i][1].size() :
					if PlayerData.player_place_group[i][1][y][0] != PlayerData.player_name :
						get_node("ec_background/VBoxContainer/ItemList").add_item(PlayerData.player_place_group[i][1][y][0])
		get_node("ec_background//VBoxContainer/ok").disabled = true


func _opponent_selected(opponent_index: int) -> void :
	opponent = get_node("ec_background/VBoxContainer/ItemList").get_item_text(opponent_index)
	get_node("ec_background/VBoxContainer/ok").disabled = false


func _valid() -> void :
	var opponent_id
	for i in PlayerData.player_place_group.size():
		for y in PlayerData.player_place_group[i][1].size():
			if PlayerData.player_place_group[i][1][y][0] == opponent :
				opponent_id = PlayerData.player_place_group[i][1][y][1]
				PlayerData.opponents.append([opponent, opponent_id])
	PlayerData.in_combat = true
	rpc_id(1, "_player_wage_combat", opponent, opponent_id, PlayerData.player_name, PlayerData.stats_concat, PlayerData.player_current_place, PlayerData.player_current_room)


### Server Funcs
@rpc("any_peer", "call_remote", "reliable")
func _player_wage_combat(new_opponent:String, opponent_id:int, player_name: String, player_stats, place_name: String, room_name: String) -> void : # Server receiving fighters
	var player_id = multiplayer.get_remote_sender_id()
	var fighters_dict = {  ## Creating dictionary to store fighters infos on ConfigFile
			"side1" : { ## Current player fighting side 
					player_name : {
						"id" : player_id,
						"stats" : player_stats,
						"ready" : false
					}
				},
			"side2" : { ## Opponent fighting side 
					new_opponent : {
						"id" : opponent_id,
						"stats" : "stats",
						"ready" : false
					}
				},
			"counter" : global.confrontation_timer,
			"confront_mode" : 0
			}
	_new_players_fighting(player_id, fighters_dict, place_name, room_name)


func _new_players_fighting(player_id: int, fighters_dict: Dictionary, place_name: String, room_name: String) -> void : ## Handling ConfigFile for confrontations
	var abord: bool = false
	var _player_fighting_dir = DirAccess.open("user://save/server")
	var current_fighting = ConfigFile.new()
	if _player_fighting_dir.file_exists(global.players_fighting_file_path): ## Checking if the opponent already is in a fight
		## Open and load the file
		current_fighting.load(global.players_fighting_file_path)
		var str_place_name: String = str(place_name)
		if current_fighting.get_sections().has(place_name):
			if place_name in current_fighting.get_sections() : ## Limitation = only one fight per room
				if room_name in current_fighting.get_section_keys(str_place_name):
					abord = true
					rpc_id(player_id, "_already_fighting")
				else:
					current_fighting.set_value(place_name, room_name, fighters_dict)
		else:
			current_fighting.set_value(place_name, room_name, fighters_dict)
	else:
		## Create the file
		current_fighting.set_value(place_name, room_name, fighters_dict)

	### Writing new datas
	current_fighting.save(global.players_fighting_file_path)

	if abord == false:
		## Verifying opponent is in the same room and getting his id
		var place = ConfigFile.new()
		var place_name_ref1: String = _reformating_place_name(place_name)
		place.load("user://save/server/places_player_tracking/"+place_name_ref1+".dat")
		var place_players: Array = place.get_value(room_name, "players")
		for i in place_players.size():
			if place_players[i][0] == fighters_dict.side2.keys()[0] :
				get_parent().get_parent().get_parent().get_parent().player_waged(place_players[i][1], fighters_dict.side1.keys()[0], player_id) #fighters_dict.side1[fighters_dict.side1.keys()[0]]["id"])
			else:
				rpc_id(player_id, "_launch_combat")


@rpc("any_peer", "call_remote")
func _give_stats_to_server(opp_name: String, opponent_stats: Dictionary, current_place: String, current_room: String) -> void: ## Server gets stats of the second fighter
	var opponent_id = multiplayer.get_remote_sender_id()
	var current_fighting = ConfigFile.new()
	current_fighting.load(global.players_fighting_file_path)
	var fighting = current_fighting.get_value(current_place, current_room)
	var new_fighting_dict: Dictionary = fighting.duplicate(true)
	for x in new_fighting_dict.keys().size():
		var new_sides: String = new_fighting_dict.keys()[x]
		if new_sides != "counter" and new_sides != "confront_mode":
			for u in new_fighting_dict[new_sides].keys().size():
				if new_fighting_dict[new_sides].keys()[u] == opp_name :
					new_fighting_dict[new_sides][opp_name].erase("stats")
					new_fighting_dict[new_sides][opp_name]["stats"] = opponent_stats
					current_fighting.set_value(current_place, current_room, new_fighting_dict)
					current_fighting.save(global.players_fighting_file_path)
### Server Funcs


@rpc("authority", "call_remote", "reliable")
func _launch_combat() -> void :
	var combat_scene = combat.instantiate()
	add_child(combat_scene)
	get_node("ec_background").visible = false

@rpc("authority", "call_remote", "reliable")
func _already_fighting() -> void :
	OS.alert("Character Already In Combat In This Area.")
	queue_free()


func check_player_presence_incombat(player_id:int, player_current_place:String, player_current_room:String) -> void: # Server func to check if a player launching the game is really in combat in this area
	var combat_allowed:bool
	var fighting = ConfigFile.new()
	fighting.load(global.players_fighting_file_path)
	if fighting.has_section(player_current_place) :
		if fighting.has_section_key(player_current_place, player_current_room):
			var current_fighting: Dictionary = fighting.get_value(player_current_place, player_current_room)
			for i in current_fighting.keys().size():
				if current_fighting.keys()[i] != "counter" && current_fighting.keys()[i] != "confront_mode":
					var side: String = current_fighting.keys()[i]
					for y in current_fighting[side].keys().size():
						var players = current_fighting[side].keys()[y]
						if current_fighting[side][players]["id"] == player_id:
							combat_allowed = true
						else:
							combat_allowed = false
	get_node("/root/connection/login/pj").get_access_to_instantcombat(player_id, combat_allowed)


func confrontation_is_over(place: String, room: String) -> void : ## Erasing confrontation in server ConfigFile (from combat_scene.gd)
	var current_fighting = ConfigFile.new()
	current_fighting.load(global.players_fighting_file_path)
	var fight_dict: Dictionary = current_fighting.get_value(place, room)
	for i in fight_dict.keys().size():
		if fight_dict.keys()[i] != "counter" and fight_dict.keys()[i] != "confront_mode":
			var sides: String = fight_dict.keys()[i]
			for y in fight_dict[sides].keys().size():
				var players: String = fight_dict[sides].keys()[y]
				rpc_id(fight_dict[sides][players]["id"], "_quit_confrontation")
	current_fighting.erase_section(place)
	current_fighting.save(global.players_fighting_file_path)


@rpc("authority", "call_remote")
func _quit_confrontation() -> void : ## Player can leave the confrontation
	print("quit confrontation")
	PlayerData.in_combat = false
	get_node("/root/connection/login/pj").action_done()
	queue_free()


### Reformating place_name if needed (adding "_" to remove spaces)
func _reformating_place_name(current_place: String) -> String:
	var place_name_ref:String
	var place_name_ref1:String
	if " " in current_place :
		place_name_ref = current_place.replace(" ", "_")
	else:
		place_name_ref = current_place
	if "'" in place_name_ref :
		place_name_ref1 = place_name_ref.replace("'", "_")
	else:
		place_name_ref1 = place_name_ref

	return place_name_ref1.to_lower()
### Reformating place_name if needed (adding "_" to remove spaces)
