extends Label

var timer_script = load("res://scenes/skills/base/timer_script.gd")
var server_timers: Array = [] ## Var to keep track of existing timers


func _ready() -> void:
	if multiplayer.get_unique_id() != 1 :
		set_process(true)
		rpc_id(1, "_request_time_left", PlayerData.player_current_place, PlayerData.player_current_room)


func _process(delta):
	if multiplayer.get_unique_id() != 1 :
		if %combat_timer.get_time_left() >= 0 :
			set_text(str(int(%combat_timer.get_time_left())))
		else:
			%combat_timer.stop()


@rpc("any_peer", "call_remote")
func _request_time_left(player_place: String, player_room: String) -> void : ## Server func to create a timer per confrontation and send the current time left to players
	var player_id: int = multiplayer.get_remote_sender_id()
	var time_left: int
	if server_timers.has(player_place+player_room) == false : ## Create new timer for a new confrontation
		var fighting = ConfigFile.new()
		fighting.load(global.players_fighting_file_path)
		var new_timer = Timer.new()
		add_child(new_timer)
		get_child(get_children().size()-1).set_name(player_place+player_room)
		server_timers.append(player_place+player_room)
		get_node(player_place+player_room).set_script(timer_script)
		get_node(player_place+player_room).ready_func()
		get_node(player_place+player_room).set_wait_time(fighting.get_value(player_place, player_room)["counter"])
		get_node(player_place+player_room).set_one_shot(true)
		get_node(player_place+player_room).set_meta("place", player_place)
		get_node(player_place+player_room).set_meta("room", player_room)
		get_node(player_place+player_room).start() ## Starting round counter on server
		
		time_left = get_node(player_place+player_room).get_time_left()
	else : ## Use already existing timer
		time_left = get_node(player_place+player_room).get_time_left()
	rpc_id(player_id, "_time_left", time_left)


@rpc("authority", "call_remote")
func _time_left(time_left: float) -> void :
	%combat_timer.set_wait_time(time_left)
	%combat_timer.start()


func end_timer(place_room: Array) -> void : ## Server func (from combat_scene.gd)
	get_node(place_room[0]+place_room[1]).stop()
	_new_turn(place_room[0], place_room[1])


func counter_ends(place, room) -> void : ## Server func to launch turn resolution when the timer server-side ends
	get_owner().on_timer_timeout(place, room)
	_new_turn(place, room)


func _new_turn(place: String, room: String) -> void : ## Server func to start a new turn
	var time_left: float
	get_node(place+room).start()

	var fighting = ConfigFile.new()
	fighting.load(global.players_fighting_file_path)
	var current_fighting: Dictionary = fighting.get_value(place, room)
	for i in current_fighting.keys().size():
		if current_fighting.keys()[i] != "counter" and current_fighting.keys()[i] != "confront_mode":
			var sides: String = current_fighting.keys()[i]
			for y in current_fighting[sides].keys().size():
				var players: String = current_fighting[sides].keys()[y]
				if ServerSave.check_online_presence_of_player_in_fight(players) == true :
					time_left = get_node(place+room).get_time_left()
					rpc_id(current_fighting[sides][players]["id"], "_time_left", time_left)
					
					
func erase_terminated_timer(timer_name: String) -> void : ## Server delete the timer from server_timers array (from timer_script.gd)
	server_timers.erase(timer_name)
