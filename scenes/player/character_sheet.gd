extends Control

var edit_descrip_main:bool = false # Track the edit state of text fields
var edit_descrip_close:bool = false # Track the edit state of text fields
var edit_notes:bool = false # Track the edit state of text fields

func _ready() -> void :
	$"%player_name".set_text(PlayerData.player_name+" - Character's Sheet")
	$%player_descr_main.set_text(PlayerData.player_name+" - Main Description")
	$%player_close.set_text(PlayerData.player_name+" - Close Description")
	$%background_label.set_text(PlayerData.player_name+" - Background")
	$%player_background.set_text(PlayerData.player_background)
	%player_description.set_text(PlayerData.player_description)
	%player_description.set_editable(false)
	%player_descr_close.set_text(PlayerData.player_description_close)
	%player_descr_close.set_editable(false)
	%player_notes.set_text(PlayerData.player_notes)
	%player_notes.set_editable(false)
#	$"%stats_titles".set_text("Stats")
#	$"%buffs".set_text("Positive Status")
#	$"%debuffs".set_text("Negative Status")
#	$"%possessed".set_text("Item Possessed")
	$"%currently_doing".set_text(PlayerData.player_name+"'s current stance: ")
	var stance = ConfigFile.new()
	stance.load("res://game_data/posture_list.cfg")
	for i in stance.get_section_keys("postures").size():
		%currently_choice_button.add_item(stance.get_section_keys("postures")[i])
		if stance.get_section_keys("postures")[i] == PlayerData.player_posture :
			%currently_choice_button.select(i)
#	$"%titles".set_text("Title Displayed")
#	$"%main".set_text("Main Actions Available")
	$"%physics".set_text("Physics Habilities")
#	$"%spells_willpower".set_text("Spells & Willpowers Habilities")
#	$"%special".set_text("Specials Habilities")
#	$"%activ_quests".set_text("Active Quests")
	$"%notes".set_text(PlayerData.player_name+" Personal Notes")
#	$"%saved_dials".set_text("Saved Dialogues")


func _on_modify_portrait_pressed() -> void :
	get_node("character_picture").popup()


func _on_modify_description_pressed() -> void :
	if edit_descrip_main == false:
		edit_descrip_main = true
		$"%player_description".set_editable(true)
	else:
		edit_descrip_main = false
		$"%player_description".set_editable(false)
		PlayerData.player_description = $"%player_description".get_text()
		PlayerSave.savegame()


func _on_modify_descr_close_pressed() -> void :
	if edit_descrip_close == false:
		edit_descrip_close = true
		$"%player_descr_close".set_editable(true)
	else:
		edit_descrip_close = false
		$"%player_descr_close".set_editable(false)
		PlayerData.player_description_close = $"%player_descr_close".get_text()
		PlayerSave.savegame()


func _on_add_notes_pressed() -> void :
	if edit_notes == false:
		edit_notes = true
		$"%player_notes".set_editable(true)
	else:
		edit_notes = false
		$"%player_notes".set_editable(false)
		PlayerData.player_notes = $"%player_notes".get_text()
		PlayerSave.savegame()


func _on_currently_choice_button_item_selected(index):
	PlayerData.player_posture = %currently_choice_button.get_item_text(index)
	PlayerSave.savegame()
