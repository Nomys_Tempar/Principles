extends Node

##### Logic
var first_connection:String = "not done" # "not done" or "done"
##### Player's variables
var player_name:String = "server"
var player_pass:String
var time_dict: Dictionary = Time.get_time_dict_from_system()
var player_sex:String = "server"
var player_description:String = "server"
var player_description_close:String
var player_background:String
var player_class: int
var player_posture:String = "server"
var player_saved_dialogs:Array = [] # PJ history saved 
var dialog_saved # Temp variable to store a dialog until it is saved on the server
var player_notes: String = ""
#var current_scene # For sound management
var locale:String = "en" # Language management variable
#var avatar_links = [] # Array of player's avatar parts, order is accessorie/body/eyes/hair/mouth
## Stats & States
var player_maxlife:int = 0
var player_currentlife:int = 0
var player_unconscious:bool = false
var player_dead:bool = false # If true the Pj is dead and no longer playable
var stats_concat:Dictionary = {} # Dict for stats management : keys = stat name
## Actions (bool or states)
var player_actions:Array # Array of actions available to the player (array of arrays [[basic actions],[spells and willpowers], [specials]])
#var travel # Travel to another place (change place)
#var search # Search the current room
## Confrontation
var in_combat:bool = false # If true character is in combat
#var allies:Array = [] # Array of allies in combat
var opponents:Array = [] # Array of opponents names
var confrontation_mode:int # 0 = strengh, 1 = will
#var turn_count: int
#var cast:Array # Array of spells and powers
## More action
#var take # Take a visible object in the current room
#var use_object:Array # Array of objects in inventory
#var equip:Array # Array of objects and weapons equipable
#var love
#var hate
#var hiding
#var hide_object
## Place related var
var player_current_place:String = "start_place"
var player_current_room
var player_place_group: Array
var player_room_group: Array
var player_known_places:Array = []
