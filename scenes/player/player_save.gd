extends Node
# Saving and loading of player's savegame and option files (original savefile creation is in crete_new_player.gd)
var previous_node
var player_data_dict: Dictionary

### Player requesting character creation
func creating_character(coming_from) -> void :
	previous_node = coming_from
	_playerdata_to_dict()
	rpc_id(1, "_save_characters_datas", player_data_dict, true)


### Player requesting savegame
func savegame() -> void :
	_playerdata_to_dict()
	rpc_id(1, "_save_characters_datas", player_data_dict, false)


func _playerdata_to_dict() -> Dictionary : # Converting PlayerData class into a dictionary we can send through RPC
	player_data_dict = {}
	var player_properties: Array = PlayerData.get_property_list()
	for i in player_properties.size():
		var current_property_dict = player_properties[i]
		var flag = PROPERTY_USAGE_SCRIPT_VARIABLE # + PROPERTY_USAGE_NIL_IS_VARIANT # Or 4096
		if current_property_dict.usage == flag or current_property_dict.usage == 135168 :
			var current_property_name = current_property_dict.name
			player_data_dict[current_property_name] = PlayerData[current_property_name]
	return(player_data_dict)


##### Savegame Func
@rpc("any_peer", "call_remote", "reliable")
func _save_characters_datas(player_data: Dictionary, new_character: bool) -> void : # The server save player's data
	var save_game = ConfigFile.new()
	if new_character == true : # No savefile so we create it
		save_game.set_value("Player ID", "player_name", player_data.player_name)
		save_game.set_value("Player ID", "player_pass", player_data.player_pass)
		save_game.set_value("Player ID", "player_sex", player_data.player_sex)
		save_game.set_value("Player ID", "player_description", player_data.player_description)
		save_game.set_value("Player ID", "player_description_close", player_data.player_description_close)
		save_game.set_value("Player ID", "player_background", player_data.player_background)
		save_game.set_value("Player States", "player_class", player_data.player_class)
		save_game.set_value("Player States", "player_posture", player_data.player_posture)
		save_game.set_value("Player States", "player_maxlife", player_data.player_maxlife)
		save_game.set_value("Player States", "player_currentlife", player_data.player_currentlife)
		save_game.set_value("Player States", "player_unconscious", player_data.player_unconscious)
		save_game.set_value("Player States", "player_dead", player_data.player_dead)
		save_game.set_value("Player Dialogs", "player_saved_dialogs", [])
		save_game.set_value("Player States", "player_stats", player_data.stats_concat)
		save_game.set_value("Player States", "player_actions", player_data.player_actions,)
		save_game.set_value("Player States", "player_current_place", global.default_place_and_room[0])
		save_game.set_value("Player States", "player_current_room", global.default_place_and_room[1])
		save_game.set_value("Player States", "player_known_places", player_data.player_known_places)
		save_game.set_value("Player States", "in_combat", player_data.in_combat)
		save_game.set_value("Player States", "opponents", player_data.opponents)
		save_game.set_value("Player ID", "player_notes", player_data.player_notes)
	else :
		### Loading of the savefile
		save_game.load("user://save/server/players/savegame_"+player_data.player_name+".dat")
	##	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.READ, player_pass)

		### Adding the new keys and values to the configfile
		save_game.set_value("Player ID", "player_name", player_data.player_name)
		save_game.set_value("Player ID", "player_pass", player_data.player_pass)
		save_game.set_value("Player ID", "player_sex", player_data.player_sex)
		save_game.set_value("Player ID", "player_description", player_data.player_description)
		save_game.set_value("Player ID", "player_description_close", player_data.player_description_close)
		save_game.set_value("Player ID", "player_background", player_data.player_background)
		save_game.set_value("Player States", "player_class", player_data.player_class)
		save_game.set_value("Player States", "player_posture", player_data.player_posture)
		save_game.set_value("Player States", "player_maxlife", player_data.player_maxlife)
		save_game.set_value("Player States", "player_currentlife", player_data.player_currentlife)
		save_game.set_value("Player States", "player_unconscious", player_data.player_unconscious)
		save_game.set_value("Player States", "player_dead", player_data.player_dead)
		save_game.set_value("Player States", "player_stats", player_data.stats_concat)
		save_game.set_value("Player States", "player_actions", player_data.player_actions,)
		save_game.set_value("Player States", "player_current_place", player_data.player_current_place)
		save_game.set_value("Player States", "player_current_room", player_data.player_current_room)
		save_game.set_value("Player States", "player_known_places", player_data.player_known_places)
		save_game.set_value("Player States", "in_combat", player_data.in_combat)
		save_game.set_value("Player States", "opponents", player_data.opponents)
		save_game.set_value("Player ID", "player_notes", player_data.player_notes)

		## Saving selected dialog history
		if player_data.dialog_saved == null : pass
		else:
			var dialog_array: Array = save_game.get_value("Player Dialogs", "player_saved_dialogs").duplicate(true)
			dialog_array.append(player_data.dialog_saved)
			save_game.set_value("Player Dialogs", "player_saved_dialogs", dialog_array)

	### Writing new datas
	save_game.save("user://save/server/players/savegame_"+player_data.player_name+".dat")

	if new_character == true :
		var character_unique_id: String = ServerSave.make_unique_character_id(PlayerData.player_name, multiplayer.get_unique_id())
		rpc_id(multiplayer.get_remote_sender_id(), "_character_creation", true, character_unique_id)


@rpc("authority", "call_remote", "reliable")
func _character_creation(valid: bool, character_unique_id:String) -> void : 
	if valid == true : ## Character creation is complete, return to the login scene
		previous_node.get_parent().queue_free()
		save_cui(character_unique_id)


func save_cui(character_unique_id:String)-> void: ## Saving character_unique_id locally on the client
	var player_cui = ConfigFile.new()
	var dir = DirAccess.open("user://save/player")
	if dir.file_exists("user://save/player/player_cui_"+PlayerData.player_name+".dat"): ## If the file already exists
		player_cui.set_value("player_cui", PlayerData.player_name, character_unique_id)
	else: ## Or create the file
		player_cui.set_value("players_cui", PlayerData.player_name, character_unique_id)
	player_cui.save("user://save/player/players_cui.dat")
#	_server_data_save("Admin "+player_name+" saved at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second))


### Saving player's texts logs
func saving_player_logs(player_name: String, new_text_node) -> void :
	var logs_dir = DirAccess.open("user://save/server/players")
	#var update_player_logs
	var current_player_logs = ConfigFile.new()
	if  logs_dir.file_exists("user://save/server/players/logs_"+player_name+".dat"):
		## Open and load the file
		current_player_logs.load("user://save/server/players/logs_"+player_name+".dat")
		if current_player_logs.has_section_key("Player Logs", player_name) :
			var player_logs = current_player_logs.get_value("Player Logs", player_name).duplicate(true)
			player_logs.append(new_text_node)
			current_player_logs.set_value("Player Logs", player_name, player_logs)
		else:
			current_player_logs[player_name] = [new_text_node]
	else:
		## Create the file
		current_player_logs.set_value("Player Logs", player_name, [new_text_node])
		### Writing new datas
	current_player_logs.save("user://save/server/players/logs_"+player_name+".dat")
### Saving players's texts logs


### Loading player's texts logs when a player join the game
func loading_player_logs(id: int, player_name: String) -> void :
	var player_logs_file = ConfigFile.new()
	var player_logs
	## Open and load the file
	var logs_dir = DirAccess.open("user://save/server/players")
	if logs_dir.file_exists("user://save/server/players/logs_"+player_name+".dat"):
		player_logs_file.load("user://save/server/players/logs_"+player_name+".dat")
		if player_logs_file.has_section_key("Player Logs", player_name):
			player_logs = player_logs_file.get_value("Player Logs", player_name)
		else:
			player_logs = null
	else:
		## Create the file
		player_logs_file.set_value("Player Logs", player_name, [])
		### Writing new datas
		player_logs_file.save("user://save/server/players/logs_"+player_name+".dat")
	get_node("/root/connection/login/pj/main_interface/HSplitContainer/text_panel/vtexts/player_livefeed").send_logs(id, player_logs)
### Loading rooms's texts logs when a player join a room or a place
