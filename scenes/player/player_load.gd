extends Node
## OLD scene to load player's data NOT USED

func _ready() -> void :
	rpc_id(1, "_load_save",PlayerData.player_name, PlayerData.player_pass) ## On pressed, we ask the server to load the savegame


@rpc("any_peer", "call_remote", "reliable")
func _load_save(player_n: String, player_pass: String) -> void : ## Server is loading player's datas & saving the player in the online_player_data file
	var id = multiplayer.get_remote_sender_id()
	var online
	var save_game = ConfigFile.new()
	var player_name = "user://save/savegame_"+player_n+".dat"

	if not save_game.file_exists(player_name):
		rpc_id(id, "_no_save_to_load")
		return # Error!  We don't have a save to load.

	### Load the file line by line and process into the dictionary to check datas
	save_game.open_encrypted_with_pass(player_name, player_pass)
	if save_game.is_open() != true : # Here we check if the player's pass matches the one saved
		rpc_id(id, "_wrong_pass")
		return # Error! Player entered the wrong password
		save_game.close()
	else : # If the password is the same...
		save_game.load(player_name)
		### ...we send the remaining variables to the player.
		rpc_id(id, "_loading_player_datas", save_game)


@rpc("any_peer", "call_remote", "reliable")
func _no_save_to_load() -> void :
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Error : no save to restore or invalid name"))


@rpc("any_peer", "call_remote", "reliable")
func _wrong_pass() -> void :
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Error : wrong password"))


### The player receive his datas...
@rpc("any_peer", "call_remote", "reliable")
func _loading_player_datas(current_save) -> void : ## dialogs
	### Opening option file to save name
	var options_file = ConfigFile.new()
	var current_option = options_file.load("user://data/options.dat")

	if options_file.has_section_key("Player Save", "player_name") :
		options_file.erase_section_key("Player Save", "player_name")
		### Adding the new keys and values to the dict
		options_file.set_value("Player Save", "player_name", current_save.player_name)
	else:
		options_file.set_value("Player Save", "player_name", current_save.player_name)
	options_file.save("user://data/options.dat")

	# Receiving variables
	PlayerData.player_name = current_save.player_name
	PlayerData.player_sex = current_save.player_sex
	PlayerData.player_OS = current_save.player_environment
	PlayerData.player_description = current_save.player_description
	PlayerData.player_description_close = current_save.player_description_close
	PlayerData.player_maxlife = current_save.maxlife
	PlayerData.player_currentlife = current_save.currentlife
	PlayerData.in_combat = current_save.in_combat
	PlayerData.player_notes = current_save.player_notes
	#PlayerData.player_current_room = current_save.player_current_room
	#PlayerData.player_current_place = current_save.player_current_place
