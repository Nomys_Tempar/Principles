extends Node
# Main Player view with Place visual, locations, chats, stats/inventory/actions, PJ and PNJ visible.

### Place scenes
const place = preload("res://scenes/places/place_main.tscn")
#var loc_routes = preload("res://scenes/places/place_routes.tscn")
### Player scenes
const player_feed = preload("res://scenes/player/feeds/player_feed.tscn")
const player_stats_overview = preload("res://scenes/player/player_stats_overview.tscn")
const player_actions = preload("res://scenes/player/player_actions.tscn")
const character_sheet = preload("res://scenes/player/character_sheet.tscn")
const travel = preload("res://scenes/skills/base/travel_scene.tscn")
const wage_combat = preload("res://scenes/skills/base/combat_management_scene.tscn")
const search_scene = preload("res://scenes/skills/base/search_scene.tscn")
const description_scene = preload("res://scenes/player/description_scene.tscn")

### Utilities
const mouse_menu = preload("res://scenes/menus/right_click_menu/click_menu_scene.tscn")

signal action_ends

func _ready() -> void :
	global.login_done = true
	if PlayerData.first_connection == "not done":
		_setup_life()
		PlayerData.first_connection = "done"
	_setup_scenes()
	find_child("player_livefeed").connect("player_saved_feed", Callable(self,"_launch_player_saved_feed"))

func _setup_scenes() -> void :
	### Loading scenes
	var mouse = mouse_menu.instantiate()
	super.add_child(mouse)

	var place_scene = place.instantiate()
	$main_interface/HSplitContainer/place_panel.add_child(place_scene)

	var stats_overview = player_stats_overview.instantiate()
	$main_interface/player_panel/vplayer.add_child(stats_overview)

	var actions = player_actions.instantiate()
	$main_interface/player_panel/vplayer.add_child(actions)

	if multiplayer.get_unique_id() == 1 :
		var wage_combat_scene = wage_combat.instantiate()
		$main_interface/HSplitContainer/place_panel.add_child(wage_combat_scene)
	if PlayerData.in_combat == true: # Player is in combat, so check with the server
		rpc_id(1, "_checking_combat_in_area", PlayerData.player_current_place, PlayerData.player_current_room)


func _setup_life() -> void :
	if PlayerData.stats_concat.has("Endurance"):
		PlayerData.player_maxlife = int(PlayerData.stats_concat.Endurance)
		PlayerData.player_currentlife = PlayerData.player_maxlife


func launch_character_sheet() -> void : # Access the character sheet screen
	var char_sheet = character_sheet.instantiate()
	add_child(char_sheet)


func _launch_player_saved_feed() -> void :
	rpc_id(1, "_get_player_saved_dialogs", PlayerData.player_name)


@rpc ("any_peer")
func _get_player_saved_dialogs(player_n: String) -> void :
	var id:int = multiplayer.get_remote_sender_id()
	var save_game = ConfigFile.new()
	save_game.load("user://save/server/players/savegame_"+player_n+".dat")
	rpc_id(id, "_receive_saved_dialog", save_game.get_value("Player Dialogs", "player_saved_dialogs"))


@rpc("authority", "call_remote", "reliable")
func _receive_saved_dialog(saved_dialog_array: Array) -> void :
	var player_feed_scene = player_feed.instantiate()
	add_child(player_feed_scene)
	get_node("player_feed").display_saved_dialogs(saved_dialog_array)


func travel_begin() -> void : ## Moving from place to place (from player_actions)
	var travel_scene = travel.instantiate()
	add_child(travel_scene)


func change_place() -> void :
	var place_scene = place.instantiate()
	$main_interface/HSplitContainer/place_panel.add_child(place_scene)


func engage_combat() -> void : ## Combat Management Screen (from player_actions)
	var wage_combat_scene = wage_combat.instantiate()
	$main_interface/HSplitContainer/place_panel.add_child(wage_combat_scene)


func player_waged(player_waged_id: int, opponent_name: String, opponent_id: int) -> void : # Server func to waged a player in combat
	rpc_id(int(player_waged_id), "_you_are_waged", opponent_name, opponent_id)


@rpc("authority", "call_remote", "reliable")
func _you_are_waged(opponent_name: String, opponent_id: int) -> void :
	PlayerData.in_combat = true
	PlayerData.opponents.append([opponent_name, opponent_id])

	var wage_combat_scene = wage_combat.instantiate()
	$main_interface/HSplitContainer/place_panel.add_child(wage_combat_scene)


## Checking if player combat is genuin
@rpc("any_peer", "call_remote", "reliable")
func _checking_combat_in_area(player_current_place: String, player_current_room: String):
	var player_id:int = multiplayer.get_remote_sender_id()
	get_node("./main_interface/HSplitContainer/place_panel/combat_management").check_player_presence_incombat(player_id, player_current_place, player_current_room)


func get_access_to_instantcombat(player_id: int, combat_allowed: bool) :
	if combat_allowed == true :
		rpc_id(player_id, "_launch_instant_combat", true)
	else:
		rpc_id(player_id, "_launch_instant_combat", false)


@rpc("authority", "call_remote", "reliable")
func _launch_instant_combat(combat:bool):
	if combat == true:
		PlayerData.in_combat = true
		engage_combat()
	else:
		PlayerData.in_combat = false
		PlayerData.opponents = []
## Checking if player combat is genuin


func search(): ## Searching place (from player_actions)
	var searching = search_scene.instantiate()
	$main_interface/HSplitContainer/place_panel.add_child(searching)


func action_done(): ## From combat management
	action_ends.emit()


func description_request(_other_name, other_id): ## from click_menu_scene/rooms_handling to display other player description
	if multiplayer.get_unique_id() == other_id:
		_receive_description(PlayerData.player_name, PlayerData.player_description_close, PlayerData.player_posture)
	else:
		rpc_id(other_id, "_request_description")


@rpc("any_peer", "call_remote")
func _request_description():
	rpc_id(multiplayer.get_remote_sender_id(), "_receive_description", PlayerData.player_name, PlayerData.player_description_close, PlayerData.player_posture)


@rpc("any_peer", "call_remote")
func _receive_description(other_name: String, other_description: String, other_posture: String): ## Receiving description and launching description scene
	var description = description_scene.instantiate()
	add_child(description)
	var stance = ConfigFile.new()
	stance.load("res://game_data/posture_list.cfg")

	get_node("description_scene/VBoxContainer/name_label").set_text(other_name+"'s Description")
	get_node("description_scene/VBoxContainer/CenterContainer/VBoxContainer/stance_label").set_text(other_name+other_posture)
	get_node("description_scene/VBoxContainer/CenterContainer/VBoxContainer/description_label").set_text(other_description)
