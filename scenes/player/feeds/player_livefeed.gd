extends Node

const text_label_scene = preload("res://scenes/player/feeds/text_label.tscn")
var narration_mode:bool = false # Narration mode provide some text in a "narration" formating (used for emotes, ingame anouncement and narration made by gms)

signal player_saved_feed

func _ready() -> void :
	%chat_target.select(0)
	%player_entry.grab_focus()

	### Loading emotes
	var emote = ConfigFile.new()
	emote.load("res://game_data/emotes.cfg")

	var emote_array: Array = emote.get_section_keys("Emote List")
	for i in emote_array.size():
		%emotes.get_popup().add_item(emote_array[i])
	%emotes.get_popup().connect("index_pressed", Callable(self,"_emotes_selected"))


func asking_for_logs() -> void :
	rpc_id(1, "_get_logs", PlayerData.player_name) # Asking the server for logs


@rpc("any_peer", "call_remote", "reliable")
func _get_logs(player_name: String) -> void : # Server checking for logs
	var id = multiplayer.get_remote_sender_id()
	PlayerSave.loading_player_logs(id, player_name)


func send_logs(id: int, player_logs) -> void : # Server sending logs back to the player
	rpc_id(id, "_receive_logs", player_logs)


@rpc("any_peer", "call_remote", "reliable")
func _receive_logs(player_logs) -> void : # Player receiving and processing logs
	if player_logs == null:
		pass
	else:
		### Clearing the livefeed
		var live_child = %live_container.get_children()
		if live_child != null:
			for y in live_child.size():
				%live_container.remove_child(live_child[y])
		else: pass
		### Loading the logs of the new room
		for i in player_logs.size():
			var text_label_instance = text_label_scene.instantiate()
			%live_container.add_child(text_label_instance)
			%live_container.get_child(%live_container.get_child_count()-1).set_text(player_logs[i])
			%live_container.move_child((%live_container.get_child(%live_container.get_child_count()-1)), 0)

func _on_text_valid_pressed() -> void :
	if %player_entry.get_text() == "" or %player_entry.get_text() == " " :
		%player_entry.clear()
	else:
		on_player_entry_text_submitted(%player_entry.get_text(), false, %chat_target.get_item_text(%chat_target.get_selected_id()))


func on_player_entry_text_submitted(chat_text: String, new_narration_mode: bool, location: String) -> void : ## Create a label node and display the text
	narration_mode = new_narration_mode
	var text_to_write: String
	if narration_mode == false :
		text_to_write = PlayerData.player_name+": "+chat_text+"\n"
	else:
		text_to_write = chat_text+"\n"
	
	if %chat_target.get_item_text(%chat_target.get_selected_id()) == "Room" or location == "Room" :
		for i in PlayerData.player_place_group.size(): # Sending player text to room
			if PlayerData.player_place_group[i][0] == PlayerData.player_current_room:
				for o in PlayerData.player_place_group[i][1].size():
					rpc_id(int(PlayerData.player_place_group[i][1][o][1]), "_receive_chat",PlayerData.player_name, chat_text, new_narration_mode)
	else:
		for x in PlayerData.player_place_group.size() : # Sending player text to place (all rooms)
			for y in PlayerData.player_place_group[x][1].size():
				rpc_id(int(PlayerData.player_place_group[x][1][y][1]), "_receive_chat", PlayerData.player_name, chat_text, new_narration_mode)
	%player_entry.clear()
	#_trigger_saving_logs(PlayerData.player_name, text_to_write) # Player save the livefeed in the current room

@rpc("any_peer", "call_local", "reliable")
func _receive_chat(player_name: String, new_chat_text: String, current_narration_mode) -> void :
	var text_label_instance = text_label_scene.instantiate()
	%live_container.add_child(text_label_instance)
	var final_text: String
	var current_text_label = %live_container.get_child(%live_container.get_child_count()-1)
	if current_narration_mode == false :
		current_text_label.set_text(player_name+": "+new_chat_text+"\n")
		final_text = player_name+": "+new_chat_text
	else:
		current_text_label.set_text("[i]"+new_chat_text+"[/i]"+"\n")
		final_text = "[i]"+new_chat_text+"[/i]"
	%live_container.move_child((current_text_label), 0)
	narration_mode = false
	_trigger_saving_logs(PlayerData.player_name, final_text) # Player save the livefeed in the current room


func _trigger_saving_logs(player_name: String, chat_text: String) -> void : # Saving the livefeed in the current room in player_save scene 
	rpc_id(1, "_saving_logs", player_name, chat_text)


@rpc("any_peer", "call_remote", "reliable")
func _saving_logs(player_name: String, new_chat_text: String) -> void :
	PlayerSave.saving_player_logs(player_name, new_chat_text)


func _emotes_selected(emote_index: int) -> void :
	narration_mode = true # Enabling narration mode to highlight emotes NOT USED
	
	var emote = ConfigFile.new()
	emote.load("res://game_data/emotes.cfg")
	var emote_text: String = emote.get_value("Emote List", emote.get_section_keys("Emote List")[emote_index])
	var formated_emote_text:String = PlayerData.player_name+" "+emote_text

	on_player_entry_text_submitted(formated_emote_text, true, "Room")


func _on_player_entry_text_changed() -> void : # We desactivate narration mode if the text is edited
	if narration_mode == true :
		narration_mode = false


func _on_saved_dialogs_pressed() -> void :
	player_saved_feed.emit() ## Onto player.gd/pj scene
