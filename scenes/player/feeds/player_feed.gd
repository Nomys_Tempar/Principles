extends Panel

func display_saved_dialogs(saved_dialog_array: Array) -> void :
	for i in saved_dialog_array.size():
		$"%saved_feed".add_text(saved_dialog_array[i])


func _on_back_pressed() -> void :
	queue_free()
