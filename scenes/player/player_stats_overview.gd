extends Panel

func _ready() -> void :
	if global.server == 0:
		$VBoxContainer/stats_title.set_text("-- "+PlayerData.player_name+"'s stats --")
		$VBoxContainer/character_sheet.set_text("Character Sheet")
#		$VBoxContainer/character_sheet.connect("pressed",Callable(self,"_on_character_sheet_pressed"))
#		$VBoxContainer/GridContainer2/posture_button.connect("item_selected",Callable(self,"_posture_changed"))

		### Setup stats names
		for i in PlayerData.stats_concat.size():
			get_node("VBoxContainer/GridContainer/stat"+str(i)).set_text(PlayerData.stats_concat.keys()[i])

		### Setup stats value
		for i in PlayerData.stats_concat.size():
			get_node("VBoxContainer/GridContainer/stat"+str(i)+"_value").set_text(PlayerData.stats_concat.get(PlayerData.stats_concat.keys()[i]))

		### Setup Posture
		var posture_file = ConfigFile.new()
		posture_file.load("res://game_data/posture_list.cfg")
		var postures: Array = posture_file.get_section_keys("postures")
		for i in postures.size():
			$VBoxContainer/GridContainer2/posture_button.add_item(postures[i])
			if postures[i] == PlayerData.player_posture:
				$VBoxContainer/GridContainer2/posture_button.select(i)


func _on_character_sheet_pressed() -> void :
	get_node("..").get_parent().get_parent().get_parent().launch_character_sheet()


func _posture_changed(new_posture_index: int) -> void :
	var posture_file = ConfigFile.new()
	posture_file.load("res://game_data/posture_list.cfg")
	PlayerData.player_posture = posture_file.get_value("postures", $VBoxContainer/GridContainer2/posture_button.get_item_text(new_posture_index))
