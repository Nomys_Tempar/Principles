extends ProgressBar

func _ready() -> void :
	add_to_group("player")
	if multiplayer.get_unique_id() != 1: # Desactivated on Server
		value = (PlayerData.player_currentlife*100)/PlayerData.player_maxlife

func wound(_wound_type, wound_value) -> void :
	PlayerData.player_currentlife -= wound_value
	print("lifebar damage")
	if PlayerData.player_currentlife <= (int(0.1*PlayerData.player_maxlife)) and PlayerData.player_currentlife > 0:
		PlayerData.player_unconscious = true
		print("player unconscious")
	elif PlayerData.player_currentlife <= 0:
		PlayerData.player_dead = true
		print("player dead")
	value = (PlayerData.player_currentlife*100)/PlayerData.player_maxlife


func heal(_heal_state) -> void :
	pass
