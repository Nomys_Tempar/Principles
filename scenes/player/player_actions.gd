extends Panel

func _ready() -> void :
	if multiplayer.get_unique_id() == 1: # On Server
		PlayerData.player_actions = [["Travel","Search","Combat","Take"],["physics_test"],["spell_test"],["special_test"]] # TO DO : Replicated action list from server configuration (GM side)

	get_node("/root/connection/login/pj").connect("action_ends", Callable(self,"_action_ends")) ## connected to player.gd (pj scene)
	
	get_node("ScrollContainer/VBoxContainer/actions_title").set_text("\n-- Available Actions --")
	$"%actions_main".set_text("\n-- Main --")
	$"%actions_physic".set_text("\n-- Physic --")
	$"%actions_spell".set_text("\n-- Spell & Willpower --")
	$"%actions_special".set_text("\n-- Special --")

	### Creating action nodes
	for x in PlayerData.player_actions.size():
		for y in PlayerData.player_actions[x].size():
			if x == 0:
				var action_button = Button.new()
				$"%GridContainer_main".add_child(action_button) # Creating buttons
				$"%GridContainer_main".get_child($"%GridContainer_main".get_child_count()-1).set_name("action"+str(y)) # Giving them names
				$"%GridContainer_main".get_child($"%GridContainer_main".get_child_count()-1).set_text_alignment(1)
			elif x == 1:
				var action_button = Button.new()
				$"%GridContainer_physic".add_child(action_button) # Creating buttons
				$"%GridContainer_physic".get_child($"%GridContainer_physic".get_child_count()-1).set_name("action"+str(y)) # Giving them names
				$"%GridContainer_physic".get_child($"%GridContainer_physic".get_child_count()-1).set_text_alignment(1)
			elif x == 2:
				var action_button = Button.new()
				$"%GridContainer_spell".add_child(action_button) # Creating buttons
				$"%GridContainer_spell".get_child($"%GridContainer_spell".get_child_count()-1).set_name("action"+str(y)) # Giving them names
				$"%GridContainer_spell".get_child($"%GridContainer_spell".get_child_count()-1).set_text_alignment(1)
			elif x == 3:
				var action_button = Button.new()
				$"%GridContainer_special".add_child(action_button) # Creating buttons
				$"%GridContainer_special".get_child($"%GridContainer_special".get_child_count()-1).set_name("action"+str(y)) # Giving them names
				$"%GridContainer_special".get_child($"%GridContainer_special".get_child_count()-1).set_text_alignment(1)
	### Creating action nodes

	### Setting up action nodes (text and visibility)
	for i in $"%GridContainer_main".get_child_count(): ## Base actions
		if $"%GridContainer_main".get_child_count() == 0:
			get_node("ScrollContainer/VBoxContainer/Control_main").visible = false
		else :
			if i < PlayerData.player_actions[0].size():
				get_node("ScrollContainer/VBoxContainer/Control_main/VBoxContainer/CenterContainer/GridContainer_main/action"+str(i)).set_text(PlayerData.player_actions[0][i])

	for i in $"%GridContainer_physic".get_child_count(): ## Physic actions
		if $"%GridContainer_physic".get_child_count() == 0:
			get_node("ScrollContainer/VBoxContainer/Control_physic").visible = false
		else:
			if i < PlayerData.player_actions[1].size():
				get_node("ScrollContainer/VBoxContainer/Control_physic/VBoxContainer/CenterContainer/GridContainer_physic/action"+str(i)).set_text(PlayerData.player_actions[1][i])

	for i in $"%GridContainer_spell".get_child_count(): ## Spell actions
		if $"%GridContainer_spell".get_child_count() == 0:
			get_node("ScrollContainer/VBoxContainer/Control_spell").visible = false
		else:
			if i < PlayerData.player_actions[2].size(): ## Spell actions
				get_node("ScrollContainer/VBoxContainer/Control_spell/VBoxContainer/CenterContainer/GridContainer_spell/action"+str(i)).set_text(PlayerData.player_actions[2][i])

	for i in $"%GridContainer_special".get_child_count(): ## Special actions
		if $"%GridContainer_special".get_child_count() == 0:
			get_node("ScrollContainer/VBoxContainer/Control_special").visible = false
		else:
			if i < PlayerData.player_actions[3].size(): ## Special actions
				get_node("ScrollContainer/VBoxContainer/Control_special/VBoxContainer/CenterContainer/GridContainer_special/action"+str(i)).set_text(PlayerData.player_actions[3][i])

	## Connection button's signals
	for u in $"%GridContainer_main".get_children().size():
		if $"%GridContainer_main".get_children()[u].get_text() == "Travel":
			$"%GridContainer_main".get_children()[u].connect("pressed",Callable(self,"_travel_pressed"))
		elif $"%GridContainer_main".get_children()[u].get_text() == "Engage Combat":
			if global.combat_allowed == false: ## If Confrontations aren't allowed in this game
				$"%GridContainer_main".get_children()[u].visible = false
			else:
				$"%GridContainer_main".get_children()[u].connect("pressed",Callable(self,"_combat_pressed"))
		elif $"%GridContainer_main".get_children()[u].get_text() == "Search":
			$"%GridContainer_main".get_children()[u].connect("pressed",Callable(self,"_search"))
		else: pass
	### Setting up action nodes (text and visibility)


func _travel_pressed() -> void :
	for i in $"%GridContainer_main".get_children().size():
		$"%GridContainer_main".get_children()[i].set_disabled(true)
	get_node("..").get_parent().get_parent().get_parent().travel_begin() # We go back to pj scene/ player.gd


func _combat_pressed() -> void :
	for i in $"%GridContainer_main".get_children().size():
		$"%GridContainer_main".get_children()[i].set_disabled(true)
	get_node("..").get_parent().get_parent().get_parent().engage_combat() # We go back to pj scene/ player.gd


func _search() -> void :
	get_node("..").get_parent().get_parent().get_parent().search()


func _action_ends():
	for i in $"%GridContainer_main".get_children().size():
		$"%GridContainer_main".get_children()[i].set_disabled(false)
