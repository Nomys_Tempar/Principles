extends Control

var class_names:Array = []
var class_description:Array = []
var class_bonuses:Array = []
var class_maluses:Array = []
var class_actions:Array = []
var class_alterate:Array = []
var stats_dict:Dictionary = {} # Dictionary with all the values regarding stats (stat_X/name_type_base_value_description)
var stats_description:Array = [] # Description text of all loaded stats
var stats_name_node_order:Array = [] # Array storing stats's nodes names 

const emit_label = preload("res://scenes/menus/emit_signal_label.tscn")
var stats_modify_script = load("res://scenes/player/new_player_creation/stats_modif_character_creation.gd")

func _ready() -> void :
	$"%welcome".set_text("Welcome to the new game!"+"\n"+"You can now create your character...")

	_load_character_classes( )# Loading character classes
	_load_base_stats() # Loading stats

	if global.points_to_add_at_creation == 0 :
		$"%VBox_points".visible = false
	else:
		$"%points_to_add".set_text(str(global.points_to_add_at_creation))


func _on_upload_pic_pressed() -> void :
	$pic_upload.popup()


func _update_sprite(file_path) -> void :
	$CenterContainer/VBoxContainer/HBoxContainer/VBox_name_pic/CenterContainer/no_pic.queue_free()

#	var texture = ImageTexture.new()
	var image = Image.new()
	image.load(file_path)
#	texture.create_from_image(image)
#	$Sprite2D.texture = texture

	$CenterContainer/VBoxContainer/HBoxContainer/VBox_name_pic/CenterContainer/character_image.texture = ImageTexture.create_from_image(image)


func _load_character_classes() -> void : # Loading character classes
	### Getting list of classes's files
	var files_array:Array = []
	var dir = DirAccess.open("res://game_data/character_class")
	dir.list_dir_begin() # TODOGODOT4 fill missing arguments https://github.com/godotengine/godot/pull/40547
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files_array.append(file)
	### Getting list of classes's files

	### Iterating in files to load names, descriptions, etc
	for i in files_array.size():
		var class_spec = ConfigFile.new()
		class_spec.load("res://game_data/character_class/"+files_array[i])

		class_names.append(class_spec.get_sections()[0])
		class_description.append(class_spec.get_value(class_spec.get_sections()[0], "description"))
		class_alterate.append(class_spec.get_value(class_spec.get_sections()[0], "stats_alterate"))
#		class_bonuses.append(class_spec.get_value(class_spec.get_sections()[0], "stats_bonuses"))
#		class_maluses.append(class_spec.get_value(class_spec.get_sections()[0], "stats_maluses"))
		class_actions.append(class_spec.get_value(class_spec.get_sections()[0], "special_actions"))
		
		#class_names.append(class_dict.character_class.name)
		#class_description.append(class_dict.character_class.description)
		#class_alterate.append(class_dict.character_class.stats_alterate)
#		class_bonuses.append(class_dict.character_class.stats_bonuses)
#		class_maluses.append(class_dict.character_class.stats_maluses)
		#class_actions.append(class_dict.character_class.special_actions)
	### Iterating in files to load names, descriptions, etc

	### Loading datas into class_button
	for i in class_names.size():
		$"%class_button".add_item(class_names[i])
	$"%class_button".select(-1)


func _load_base_stats() -> void :
	var stats_to_load = ConfigFile.new()
	stats_to_load.load("res://game_data/stats.cfg")

	## Conversion of the ConfigFile into dictionary
	var stats_sections:Array = stats_to_load.get_sections()
	for x in stats_sections.size():
		var stats_dict_keys:Dictionary={}
		var stats_keys:Array = stats_to_load.get_section_keys(stats_sections[x])
		for y in stats_keys.size():
			stats_dict_keys[stats_keys[y]] = stats_to_load.get_value(stats_sections[x], stats_keys[y])
		stats_dict[stats_sections[x]] = stats_dict_keys
	## Conversion of the ConfigFile into dictionary
	for i in stats_dict.keys().size():
		var current_key = stats_dict.keys()[i]
		### Creating label for the stat name
		var stat_label = emit_label.instantiate()
		$"%stats_grid".add_child(stat_label) 
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_name("stat_name"+str(i)) # Giving them names
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_text(stats_dict[current_key].name)
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).connect("node_name",Callable(self,"_get_hovered_node_name"))
		stats_name_node_order.append($"%stats_grid".get_child($"%stats_grid".get_child_count()-1).get_text()) # Incrementing the array storing stats name
		### Creating label for the stat name

		### Creating button to add points
		var stat_add = Button.new()
		$"%stats_grid".add_child(stat_add) 
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_name("stat_add"+str(i)) # Giving them names
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_text("+")
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_script(stats_modify_script) # Loading node's script to handle value changes
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_owner(self) # Setting owner to get easy access to the scene's nodes
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).connect("pressed",Callable($"%stats_grid".get_child($"%stats_grid".get_child_count()-1),"_add_point").bind($"%stats_grid".get_child($"%stats_grid".get_child_count()-1).get_index()))
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1)._ready() # Launch the script
		### Creating button to add points

		### Creating label for the stat value
		var stat_value_label = Label.new()
		$"%stats_grid".add_child(stat_value_label) 
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_name("stat_value"+str(i)) # Giving them names
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_text(str(stats_dict[current_key].base_value))
		### Creating label for the stat value

		### Each stat's description is stored the dedicated array
		stats_description.append(stats_dict[current_key].description)

		### Creating button to remove points
		var stat_remove = Button.new()
		$"%stats_grid".add_child(stat_remove)
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_name("stat_remove"+str(i)) # Giving them names
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_text("-")
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_script(stats_modify_script) # Loading node's script to handle value changes
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).set_owner(self) # Setting owner to get easy access to the scene's nodes
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1).connect("pressed",Callable($"%stats_grid".get_child($"%stats_grid".get_child_count()-1),"_remove_point").bind($"%stats_grid".get_child($"%stats_grid".get_child_count()-1).get_index()))
		$"%stats_grid".get_child($"%stats_grid".get_child_count()-1)._ready() # Launch the script
		### Creating button to remove points


func _get_hovered_node_name(node_name: String) -> void :
	if "stat" in node_name:
		for i in stats_description.size() :
			if node_name.ends_with(str(i)):
				$"%hover_description".display_description(stats_description[i])


func _on_class_button_item_selected(class_number: int) -> void :
	$"%class_descr".set_text(class_description[class_number]) # Class description

	$"%class_bonus".clear() # Class bonus display
	$"%class_malus".clear()  # Class malus  display

	### Handling alter-stats display and addition to stats
	for u in stats_name_node_order.size(): # Checking the addition node ("+" and "-")
		for i in class_alterate[class_number].size():
			if class_alterate[class_number][i][0] == stats_name_node_order[u]: # Finding the right one
				if class_alterate[class_number][i][1] > 0 : # Alter positif (bonus)
					$"%class_bonus".add_text(class_alterate[class_number][i][0]+" +"+str(class_alterate[class_number][i][1])+"\n") # Display
					get_node("%stats_grid/stat_add"+str(u))._add_alter(class_alterate[class_number][i][1], get_node("%stats_grid/stat_add"+str(u)).get_index()) # Applying bonuses to the stat via the "+" button
				elif class_alterate[class_number][i][1] < 0 : # Alter negatif (malus)
					$"%class_malus".add_text(class_alterate[class_number][i][0]+" "+str(class_alterate[class_number][i][1])+"\n") # Display
					get_node("%stats_grid/stat_add"+str(u))._add_alter(class_alterate[class_number][i][1], get_node("%stats_grid/stat_add"+str(u)).get_index()) # Applying maluses to the stat via the "+" button
				else:
					get_node("%stats_grid/stat_add"+str(u))._add_alter(0, get_node("%stats_grid/stat_add"+str(u)).get_index()) # Reset stat via the "+" button if alter = 0

	for u in $"%class_action_grid".get_child_count(): # Class special actions display
		$"%class_action_grid".get_child(u).queue_free()
	if class_actions[class_number].is_empty() == true:
		var action_label = Label.new()
		$"%class_action_grid".add_child(action_label) # Creating label
		$"%class_action_grid".get_child($"%class_action_grid".get_child_count()-1).set_name("action none") # Giving them names
		$"%class_action_grid".get_child($"%class_action_grid".get_child_count()-1).set_text("None")
	else:
		for t in class_actions[class_number].size():
			var action_label = emit_label.instantiate()
			$"%class_action_grid".add_child(action_label) # Creating label
			$"%class_action_grid".get_child($"%class_action_grid".get_child_count()-1).set_name("action"+str(t)) # Giving them names
			$"%class_action_grid".get_child($"%class_action_grid".get_child_count()-1).set_text(class_actions[class_number][t])
			$"%class_action_grid".get_child($"%class_action_grid".get_child_count()-1).connect("node_name",Callable(self,"_get_hovered_node_name"))


func _create_is_hovered() -> void : # Verifiying if all fields are answered
	if $"%points_to_add".get_text() != "0":
		$"%hover_description".set_text("You still have Skill Points to use.")
		$CenterContainer/VBoxContainer/create_character.disabled = true
	elif $CenterContainer/VBoxContainer/HBoxContainer/VBox_name_pic/GridContainer/name_text.get_text() == "" or $CenterContainer/VBoxContainer/HBoxContainer/VBox_name_pic/GridContainer/pass_text.get_text() == "" :
		$"%hover_description".set_text("Character Name or password empty.")
		$CenterContainer/VBoxContainer/create_character.disabled = true
#	elif $CenterContainer/VBoxContainer/HBoxContainer/VBox_name_pic/CenterContainer/character_image.get_texture() == null :
#		$"%hover_description".set_text("No picture selected.")
#		$CenterContainer/VBoxContainer/create_character.disabled = true
	elif $CenterContainer/VBoxContainer/HBoxContainer/VBox_desc/descr.get_text() == "" or $CenterContainer/VBoxContainer/HBoxContainer/VBox_desc/descr_close.get_text() == "" or $CenterContainer/VBoxContainer/HBoxContainer/VBox_desc/charac_backgound.get_text() == "":
		$"%hover_description".set_text("Description field empty.")
		$CenterContainer/VBoxContainer/create_character.disabled = true
	else : # If everything is set we can click on the button to create the character
		get_node("..").get_parent().verif_new_player_name($CenterContainer/VBoxContainer/HBoxContainer/VBox_name_pic/GridContainer/name_text.get_text())


func is_character_exist(valid: int) -> void : # valid is 0 if the name is valid, valid is 1 is the name is already taken by another character
	if valid == 1:
		$"%hover_description".set_text("Character with this name already exists.")
		$CenterContainer/VBoxContainer/create_character.disabled = true
	else:
		$CenterContainer/VBoxContainer/create_character.disabled = false


func _create_character() -> void :
	PlayerData.player_name = $CenterContainer/VBoxContainer/HBoxContainer/VBox_name_pic/GridContainer/name_text.get_text()
	PlayerData.player_pass = $CenterContainer/VBoxContainer/HBoxContainer/VBox_name_pic/GridContainer/pass_text.get_text()
	PlayerData.player_sex = "" # TO DO
	PlayerData.player_posture = "is standing."
	PlayerData.player_description = $CenterContainer/VBoxContainer/HBoxContainer/VBox_desc/descr.get_text()
	PlayerData.player_description_close = $CenterContainer/VBoxContainer/HBoxContainer/VBox_desc/descr_close.get_text()
	PlayerData.player_background = $CenterContainer/VBoxContainer/HBoxContainer/VBox_desc/charac_backgound.get_text()
	PlayerData.player_class = $"%class_button".get_selected()
	PlayerData.player_actions = [["Travel","Search","Engage Combat"],[],[],[]]

	for i in $"%stats_grid".get_children().size():
		for u in stats_dict.keys().size():
			if $"%stats_grid".has_node("stat_name"+str(i)) == true and get_node("%stats_grid/stat_name"+str(i)).get_text().matchn(stats_dict[stats_dict.keys()[u]].name) == true:
				PlayerData.stats_concat[stats_dict[stats_dict.keys()[u]].name] = get_node("%stats_grid/stat_value"+str(i)).get_text()

	var coming_from = get_node(".")
	PlayerSave.creating_character(coming_from)
