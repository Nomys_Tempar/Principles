extends Node

var stats_dict:Dictionary = {}
var bonus:int = 0
var malus:int = 0
var old_alter:int = 0

func _ready() -> void :
	## Loading Stats cfg file
	var stats = ConfigFile.new() 
	stats.load("res://game_data/stats.cfg")
	var sections_array: Array = stats.get_sections()
	for i in sections_array.size():
	# Fetch the data for each section.
		var keys_array: Array = stats.get_section_keys(sections_array[i])
		var stats_dict_section: Dictionary = {}
		for x in keys_array.size():
			stats_dict_section[keys_array[x]] = stats.get_value(sections_array[i], keys_array[x])
		stats_dict[sections_array[i]] = stats_dict_section


func _add_point(clicked_index: int) -> void : # Adding points by clicling on the button
	if get_owner().get_node("%points_to_add").get_text() == "0":
		get_owner().get_node("%hover_description").display_description("No more points to add.")
	else:
		get_parent().get_children()[clicked_index+1].set_text(str(int(get_parent().get_children()[clicked_index+1].get_text())+1)) # Adding points
		get_owner().get_node("%points_to_add").set_text(str(int(get_owner().get_node("%points_to_add").get_text())-1))


func _remove_point(clicked_index: int) -> void : # Removing points by clicling on the button
	if get_parent().get_children()[clicked_index-1].get_text() <= str(stats_dict.stat_1.base_value):
		get_owner().get_node("%hover_description").display_description("Can't remove more points.")
	else:
		get_parent().get_children()[clicked_index-1].set_text(str(int(get_parent().get_children()[clicked_index-1].get_text())-1))
		get_owner().get_node("%points_to_add").set_text(str(int(get_owner().get_node("%points_to_add").get_text())+1))


func _add_alter(new_alter, clicked_index: int) -> void : # When a class is selected alter are applying to the stat
	if old_alter != 0 :
		get_parent().get_children()[clicked_index+1].set_text(str(int(get_parent().get_children()[clicked_index+1].get_text())+(old_alter*(-1))))
		old_alter = 0

	get_parent().get_children()[clicked_index+1].set_text(str(int(get_parent().get_children()[clicked_index+1].get_text())+new_alter))
	old_alter = new_alter
