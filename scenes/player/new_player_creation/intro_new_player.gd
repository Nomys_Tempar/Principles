extends Control

const creation_screen1 = preload("res://scenes/player/new_player_creation/creation_scene1.tscn")

func _ready() -> void :
	if global.dev == true:
		_on_next_pressed()


func _on_next_pressed() -> void :
	var creation1 = creation_screen1.instantiate()
	add_child(creation1)
