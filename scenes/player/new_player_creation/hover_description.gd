extends RichTextLabel

func display_description(description_text: String) -> void :
	clear()
	add_text(description_text)
