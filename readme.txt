Principles is a framework for multiplayer RPG made by Nomys_Tempar, with Godot Engine.

It's main goal is to provide a successor for the Talesta php engine for playing online RPGs.

Principles is published under Libre Art Licence 1.3 https://artlibre.org

Please read the Todo.txt file to know more about available features. 